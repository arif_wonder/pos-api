<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthClient extends Model {
	protected $connection = 'auth_login';
	protected $table = 'clients';
	protected $guarded = [];
}
