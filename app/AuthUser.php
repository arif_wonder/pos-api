<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class AuthUser extends Authenticatable implements JWTSubject
{
	use Notifiable;

	protected $connection = 'auth_login';
	protected $table = 'users_login';
	protected $guarded = [];

	protected $hidden = [
		'password', 'api_token', 'raw_password', 'client',
	];

	protected $appends = ['db_info', 'store_detail', 'change_pswd'];

	public function getJWTIdentifier() {
		return $this->getKey();
	}

	public function getJWTCustomClaims() {
		return [];
	}

	public function client() {
		return $this->belongsTo('App\AuthClient');
	}

	public function user() {
		return $this->belongsTo('App\User');
	}

	public function getDbInfoAttribute() {
		$info = $this->client ? $this->client->db_info : null;
		if ($info) {
			$dbInfo = unserialize(base64_decode($info));
			$newDbInfo = [];
			if (is_array($dbInfo)) {
				foreach ($dbInfo as $key => $value) {
					$newDbInfo[$key] = \Crypt::decryptString($value);
				}
			}
			return $newDbInfo;
		}
		return null;
	}

	public function getStoreDetailAttribute() {
		$data = collect();
		$client = $this->client;
		if ($client) {
			$data->put('max_stores', $client->max_stores);
			$data->put('created_stores', $client->created_stores);
			$data->put('available_limit', $client->max_stores - $client->created_stores);
		}
		return $data;
	}

	public function getChangePswdAttribute() {
		$client = $this->client;
		if ($client->id == $this->client_id) {
			return $client->change_pswd;
		}
		return 0;
    }

}
