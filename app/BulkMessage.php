<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BulkMessage extends Model {
	protected $connection = 'mysql';
	protected $fillable = ['customer_phones', 'message'];
}
