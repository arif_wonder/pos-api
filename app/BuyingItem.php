<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuyingItem extends Model {
	protected $connection = 'mysql';

	protected $table = 'buying_item';

	protected $guarded = [];

	public $timestamps = false;
}
