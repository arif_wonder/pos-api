<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model {
	protected $connection = 'mysql';

	protected $guarded = [];

	protected $primaryKey = 'cart_id';

	protected $appends = ['products', 'total_price', 'count', 'total_tax', 'total_discount'];

	public function products() {
		return $this->hasMany('App\CartProduct', 'cart_id', 'cart_id');
	}

	public function getProductsAttribute() {
		return $this->products()->get();
	}

	public function getTotalPriceAttribute() {
		$prds = $this->products; //()->get();
		if ($prds) {
			$amt = 0;
			foreach ($prds as $prd) {
				$amt += $prd->price_after_discount;
			}
			return round($amt, 2);
		}
	}

	public function getCountAttribute() {
		return $this->products()->count();
	}

	public function getTotalTaxAttribute() {
		$prds = $this->products;
		if ($prds) {
			$tax = 0;
			foreach ($prds as $prd) {
				$tax += $prd->tax;
			}
			return round($tax, 2);
		}
		return 0;
	}

	public function getTotalDiscountAttribute() {
		$products = $this->products;
		$totalDisc = 0;
		if ($products) {
			foreach ($products as $prd) {
				$totalDisc += $prd->discount;
			}
		}
		return round($totalDisc, 2);
	}
}
