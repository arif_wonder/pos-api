<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartProduct extends Model {
	protected $connection = 'mysql';

	protected $guarded = [];

	public $timestamps = false;

	protected $appends = ['product', 'total_price', 'tax', 'discount', 'discount_detail', 'price_after_discount'];

	public function product() {
		return $this->belongsTo('App\Product', 'product_id', 'p_id');
	}

	public function getProductAttribute() {
		return $this->product()->find($this->product_id);
	}

	public function getTotalPriceAttribute() {
		$product = $this->product;
		$total_price = 0;
		if ($product) {
			$total_price = $this->quantity * $product->selling_price;
		}
		return $total_price;
	}

	public function getDiscountDetailAttribute() {
        $product = $this->product;
        $product2 = $product->toArray();
        $discount = [];
		if ($product->is_gift_item) {
			$dsc['discount'] = $product->selling_price;
			$dsc['discount_type'] = 'fixed';
			$dsc['total_discount'] = round($this->quantity * $product->selling_price, 2);
            $discount['flat'] = $dsc;
			return $discount;
		}
		if ($product && $product->p_discount) {
			$dsc['discount'] = $product2['p_discount'];
			$dsc['discount_type'] = $product2['p_discount_type'];
			$dsc['total_discount'] = round($this->quantity * $product2['p_discount'], 2);
            $discount['flat'] = $dsc;
		}
		if ($product && $product->cat_discount) {
            $ct = $product2['cat_discount'];
			$dsc['discount'] = $ct['discount'];
			$dsc['discount_type'] = $ct['discount_type'];
			$dsc['total_discount'] = min(round($this->quantity * $ct['amount'], 2), $ct['max_discount']);
			$dsc['min_purchase_amount'] = $ct['min_purchase_amount'];
            $discount['category'] = $dsc;
		}
		if ($product && $product->hh_discount) {
            $hh = $product->hh_discount;
			$dsc['discount'] = $hh->discount;
			$dsc['discount_type'] = $hh->discount_type;
			$dsc['total_discount'] = min(round($this->quantity * $hh->amount, 2), $hh->max_discount);
			$dsc['min_purchase_amount'] = $hh->min_purchase_amount;
			$discount['happy_hour'] = $dsc;
		}
		return $discount;
	}

	public function getDiscountAttribute() {
		$discount = $this->discount_detail;
		$discAmount = 0;
		$product = $this->product;
		foreach ($discount as $disc) {
			$discAmount += $disc['total_discount'];
		}
		// return ($discAmount > $product->selling_price) ? 0 : $discAmount;
		return $discAmount;
	}

	public function getPriceAfterDiscountAttribute() {
		return $this->total_price - $this->discount;
	}

	public function getTaxAttribute() {
		$prd = $this->product;
		$tax = 0;
		if ($prd) {
			// $taxablePrice = $this->total_price - $this->discount;
			$basePrice = ($this->price_after_discount * 100) / ($prd->p_taxrate + 100);
			$tax = $this->price_after_discount - $basePrice;
		}
		return round($tax, 2);
	}
}
