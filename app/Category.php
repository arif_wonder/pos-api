<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
	protected $connection = 'mysql';

	protected $table = 'categorys';
	protected $primaryKey = 'category_id';

	public function discount() {
		return $this->hasOne('App\DiscountByCategory', 'category_id', 'category_id');
	}
}
