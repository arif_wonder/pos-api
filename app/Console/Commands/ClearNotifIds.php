<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ClearNotifIds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all unused notification ids.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $usedNotifs = \App\SellingPrice::whereNotNull('payment_id')->pluck('payment_id')->all();
        \App\P2P_Notif::whereNotIn('notif_id', $usedNotifs)->delete();
    }
}
