<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model {
	protected $connection = 'mysql';

	protected $table = 'disc_coupons';
	protected $guarded = [];
	public $timestaps = false;
}
