<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponCustomer extends Model {
	protected $connection = 'mysql';

	protected $guarded = [];
	public $timestamps = false;
}
