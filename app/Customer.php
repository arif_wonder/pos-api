<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model {
	protected $connection = 'mysql';

	protected $fillable = ['customer_name', 'customer_email', 'customer_mobile', 'customer_sex', 'customer_age', 'customer_address', 'anniversary_date', 'occupation', 'dob', 'marital_status', 'no_of_children', 'alt_contact_number', 'passport_number', 'dl_number', 'pan_number', 'aadhar_number', 'credit_balance'];

	protected $primaryKey = 'customer_id';

	public function stores() {
		return $this->hasOne('App\StoreCustomer', 'customer_id', 'customer_id');
	}
}
