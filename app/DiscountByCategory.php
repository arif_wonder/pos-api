<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountByCategory extends Model {
	protected $connection = 'mysql';

	public $timestamps = false;
	protected $table = 'disc_by_category';

}
