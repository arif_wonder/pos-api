<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountHappyHour extends Model {
	protected $connection = 'mysql';

	public $timestamps = false;
	protected $table = 'disc_happy_hours';

}
