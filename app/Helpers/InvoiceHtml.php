<?php
namespace App\Helpers;

use Anam\PhantomMagick\Converter;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class InvoiceHtml {
	private static $location = 'invoices';

	public static function generateInvoice($invoiceId) {
		$user = \JWTAuth::user();
		if ($user && $user->user) {
			$user = $user->user;
		}
		$storeId = $user->store->store_id;
		$html = self::getInvoiceHtml($invoiceId);
		$fileName = $user->id . $storeId . $invoiceId . '.png';
		$filePath = public_path('temp') . '/' . $fileName;
		$conv = new Converter();
		$conv->addPage($html)
			->toPng()
			->imageOptions([
				'width' => 350,
				'height' => 1500,
				'quality' => 100,
			])
			->save($filePath);
		if (\File::isFile($filePath)) {
			$path = Storage::putFile(self::$location, new File($filePath), 'public');
			@unlink($filePath);
			$invUrl = Storage::url($path);
			$invInfo = \App\SellingInfo::where('invoice_id', $invoiceId)->first();
			$invInfo->invoice_url = $invUrl;
			$invInfo->save();
			return $invUrl;
		}
		return false;
	}

	public static function getInvoiceHtml($invoiceId) {
		$user = \JWTAuth::user();
		if ($user && $user->user) {
			$user = $user->user;
		}
		$store = $user->store ? $user->store->store : null;
		if (!$store) {
			return null;
		}
		$invoiceData = \App\Helpers\Util::getInvoiceInfo($invoiceId, $store->store_id);
		$payMethod = \App\Payment::find($invoiceData->payment_method);
		$logoURL = $store->logo ? env('STORE_URL') . 'assets/wonderpillars/img/logo-favicons/' . $store->logo : asset('logo.png');
		$poweredLogo = asset('footer_logo.png');
		$crDate = \Carbon\Carbon::parse($invoiceData->created_at)->format('d M Y h:i A');
		$address = $store->address . ', ' . $store->area . ', ' . $store->city;

		$cashier = $invoiceData->cashier;
		$footerText = $store->preference ? $store->preference['invoice_footer_text'] : 'Thank you for chosing us!';

		$products = \App\Helpers\Util::getInvoiceItems($invoiceId);

		$productRow = '';
		$sn = 1;
		$subtotal = $itemDisc = 0;
		$gst = [];
		$isIgst = $store->gst_type == 'IGST' ? true : false;
		foreach ($products as $prod) {
			$productOrig = \App\Product::find($prod->item_id);

			$rate = (int) $productOrig->p_taxrate;
			$itemTaxable = ($productOrig->selling_price * 100) / ($productOrig->p_taxrate + 100);
			$itemTax = $productOrig->selling_price - $itemTaxable;
			$taxable = $prod->price_after_discount - $prod->item_tax;
			$tax = $prod->item_tax;
			$newArr = [
				'rate' => $productOrig->p_taxrate,
				'taxable' => isset($gst[$rate]['taxable']) ? $gst[$rate]['taxable'] + $taxable : $taxable,
				'tax' => isset($gst[$rate]['tax']) ? $gst[$rate]['tax'] + $tax : $tax,
			];
			$gst[$rate] = $newArr;

			$itemDisc += $productOrig->p_discount * $prod->item_quantity;
			$sgst = $cgst = $igst = 0;
			if ($isIgst) {
				$igst = number_format($productOrig->p_taxrate, 2, '.', '');
			} else {
				// $sgst = $cgst = $productOrig->p_taxrate / 2;
				$sgst = $cgst = number_format(($productOrig->p_taxrate / 2), 2, '.', '');
			}

			// $amount = ($prod->item_price - $productOrig->p_discount) * $prod->item_quantity;
			$subtotal += $prod->price_after_discount;
			$amount = number_format($prod->price_after_discount, 2, '.', '');

			$productRow .= '<tr><td colspan="5"><p style="text-align:left; font-weight:600">' . $prod->item_name . '</p></td></tr>';
			$productRow .= '<tr>
                        <td>
                            <p style="text-align:center; font-weight:600;">' . $sn . '</p>
                        </td>
                        <td>
                            <p style="text-align:center; font-weight:600;"> ' . $prod->item_quantity . '<br/>' . $productOrig->p_unit . ' </p>
                        </td>';
			if ($isIgst) {
				$productRow .= '<td><p style="text-align:center; font-weight:600;">' . number_format($prod->item_price, 2, '.', '') . '<br/>&nbsp;</p></td><td><p style="text-align:center; font-weight:600;"> ' . number_format($prod->item_discount, 2, '.', '') . ' <br>' . $igst . '  </p></td>';
			} else {
				$productRow .= '<td><p style="text-align:center; font-weight:600;">' . number_format($prod->item_price, 2, '.', '') . ' <br>' . $cgst . '  </p></td><td><p style="text-align:center; font-weight:600;"> ' . number_format($prod->item_discount, 2, '.', '') . ' <br>' . $sgst . '  </p></td>';
			}
			$productRow .= '<td>
                            <p style="text-align:right; font-weight:600;"> ' . $amount . '<br/>&nbsp;</p>
                        </td>
                    </tr>';
			$sn++;
		}
		$gstRow = '';
		if (count($gst) > 0) {
			foreach ($gst as $arr) {
				$gstRow .= '<tr>
                            <td>
                                <p style="text-align:center; font-weight:600;"> ' . $arr['rate'] . ' </p>
                            </td>
                            <td>
                                <p style="text-align:center; font-weight:600;"> ' . round($arr['taxable'], 2) . ' </p>
                            </td>
                            <td>
                                <p style="text-align:center; font-weight:600;">' . round($arr['tax'], 2) . ' </p>
                            </td>
                        </tr>';
			}
		}

		$promDisc = number_format($invoiceData->discount_amount, 2, '.', '');
		// $payableAmt = number_format(0, 2);
		$payableAmt = number_format(((float) $invoiceData->gross_total + (float) $invoiceData->previous_due - ((float) $promDisc + (float) $invoiceData->paid_by_credit)), 2, '.', '');
		// dd($payableAmt);
		$prevDue = number_format($invoiceData->previous_due, 2, '.', '');
		$paidByCredit = number_format($invoiceData->paid_by_credit, 2, '.', '');
		$paidAmt = number_format(($invoiceData->paid_amount - $invoiceData->paid_by_credit), 2, '.', '');
		$presentDue = number_format($invoiceData->present_due, 2, '.', '');
		$grossTotal = number_format($invoiceData->gross_total, 2, '.', '');

		$tableHead = '';
		if ($isIgst) {
			$tableHead = '<tr style="border-bottom:dashed 1px #eee; border-top:dashed 1px #eee;">
                            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c;">S. No.</th>
                            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c;"> Qty. <br>Unit </th>
                            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c;"> MRP(Rs)</th>
                            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c;"> Disc.(Rs) <br/>IGST% </th>
                            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c; text-align:right;"> Amount(Rs) <br>(Incl. GST) </th>
                        </tr>';
		} else {
			$tableHead = '<tr style="border-bottom:dashed 1px #eee; border-top:dashed 1px #eee;">
                            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c;">S. No.</th>
                            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c;"> Qty. <br>Unit </th>
                            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c;"> MRP(Rs) <br>CGST% </th>
                            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c;"> Disc.(Rs) <br>SGST% </th>
                            <th style="border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c; text-align:right;"> Amount(Rs) <br>(Incl. GST) </th>
                        </tr>';
		}

		$html = <<<EOT
<html xmlns="http://www.w3.org/1999/xhtml"><head> <meta http-equiv="Content-type" content="text/html; charset=utf-8"/> <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/> <title>MOBIOCEAN </title><!--[if gte mso 9]> <style type="text/css" media="all"> sup{font-size: 100% !important;}</style><![endif]--> <style type="text/css" media="screen"> body,p{padding:0!important;margin:0!important}body{display:block!important;min-width:100%!important;width:100%!important;-webkit-text-size-adjust:none;font-family:arial}a{color:#00349d;font-weight:700;text-decoration:none}p{line-height:22px;font-size:13px}img{-ms-interpolation-mode:bicubic}.mcnPreviewText{display:none!important}h3,h4,h5,h6{padding:0;margin:0}.qtd1 td{font-size:12px;text-align:center}</style></head><body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; font-family:arial; width:100% !important; background:#fff; -webkit-text-size-adjust:none;">
    <table width="320px" border="0" cellspacing="0" cellpadding="0" style="margin:auto; margin-top:10px;">
   <tr>
      <td align="center" colspan="2"><img src="$logoURL" style="width:150px;"></td>
   </tr>
   <tr>
      <td align="center" colspan="2">
         <br>
         <h4 style="margin-bottom:6px; padding:0px; margin:0px;">{$store->name}</h4>
         <h6 style="margin-bottom:6px; padding:0px; margin:0px;">$address</h6>
         <h6 style="margin-bottom:6px; padding:0px; margin:0px;">Invoice ID:  $invoiceId </h6>
         <h6 style="margin-bottom:6px; padding:0px; margin:0px;">GST Number:  {$store->vat_reg_no} </h6>
      </td>
   </tr>
   <tr>
      <td align="center" colspan="2">
         <h5 style="margin-top:2px;">Date: $crDate</h5>
      </td>
   </tr>
   <tr>
      <td style="text-align:left;">
         <h6> Cashier Name - $cashier</h6>
      </td>
      <td style="text-align:right;">
         <h6> Customer Name - {$invoiceData->customer_name} <br>Mobile - {$invoiceData->customer_mobile} </h6>
      </td>
   </tr>
</table>
<table width="320px" border="0" cellspacing="3" cellpadding="3" style="margin:auto; margin-top:10px; font-size:12px;">
   $tableHead
    $productRow
</table>
<table width="320px" border="0" cellspacing="2" cellpadding="2" style="margin:auto; margin-top:10px; border-bottom:dashed 1px #3c3c3c; border-top:dashed 1px #3c3c3c; text-align:right; font-weight:600; font-size:12px;">
   <tbody>
      <tr>
         <td> Gross Total </td>
         <td>  $grossTotal </td>
      </tr>
      <tr>
         <td> Total Discount(-) </td>
         <td>  $promDisc </td>
      </tr>
      <tr>
         <td> Previous Due(+) </td>
         <td>  $prevDue </td>
      </tr>
      <tr>
         <td> Credit Used(-) </td>
         <td>  $paidByCredit </td>
      </tr>
      <tr>
         <td style=" border-top:dashed 1px #3c3c3c;"> Payable Amount </td>
         <td style=" border-top:dashed 1px #3c3c3c;">  $payableAmt </td>
      </tr>
      <tr>
         <td> Paid Amount </td>
         <td> $paidAmt </td>
      </tr>
      <tr>
         <td> Present Dues </td>
         <td> $presentDue </td>
      </tr>
      <tr>
         <td> Payment Mode </td>
         <td> $payMethod->name </td>
      </tr>
   </tbody>
</table>
<table width="320px" border="0" cellspacing="0" cellpadding="0" style="margin:auto; margin-top:10px; border-bottom:dashed 1px #3c3c3c; text-align:right; font-weight:600; font-size:12px;">
   <tbody>
      <tr>
         <td colspan="3">
            <h5 style="font-size:16px; text-align:center;">GST Details</h5>
         </td>
      </tr>
      <tr>
      <tr>
         <td>
            <p style="text-align:center; font-weight:600;"> GST% </p>
         </td>
         <td>
            <p style="text-align:center; font-weight:600;"> Taxable </p>
         </td>
         <td>
            <p style="text-align:center; font-weight:600;"> Total TAX </p>
         </td>
      </tr>
       $gstRow
      </tr>
      <tr>
         <td colspan="3" style="text-align:center; padding-bottom:2px; border-top:dashed 1px #3c3c3c; ">
            <p style=""> $footerText </p>
         </td>
      </tr>
   </tbody>
</table>
 <br><div align="center"><img src="$poweredLogo" style="width:150px;"></div><br></body></html>
EOT;
		return $html;
	}
}
