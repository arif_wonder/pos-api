<?php
namespace App\Helpers;

class MyDBHelper {
	public static function setDBConnection($loggedInUser = null) {
		$user = $loggedInUser ?: \JWTAuth::user();
		if ($user && $dbInfo = $user->db_info) {
			\Config::set('database.connections.mysql', array(
				'driver' => 'mysql',
				'host' => $dbInfo['host'],
				'database' => $dbInfo['db'],
				'username' => $dbInfo['user'],
				'password' => $dbInfo['pass'],
				'charset' => 'utf8',
				'collation' => 'utf8_general_ci',
				'prefix' => '',
			));
		} else {
			return response()->json(['status' => false, 'message' => 'Invalid request'], 400);
		}
	}
}
