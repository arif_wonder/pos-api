<?php

namespace App\Helpers;

use JWTAuth;

class Util {
	/**
	 * Get current user's order payable price
	 *
	 * @param Illuminata\Http\Request $request
	 * @return Illuminate\Http\Response
	 */
	public static function getCurrentOrderPayablePrice(\Illuminate\Http\Request $request) {
		$result = Util::implementCategoryDiscount($request);
		return Util::implementHappyHourDiscount($result);
	}

	public static function implementCategoryDiscount($request) {
		$now = date('Y-m-d');
		$user = JWTAuth::user();
		if ($user && $user->user) {
			$user = $user->user;
		}
		$products = [];
		//make item by category
		foreach ($user->cart->products as $product) {
			if (!isset($products[$product->product->category_id])) {
				$products[$product->product->category_id] = [];
			}
			array_push($products[$product->product->category_id], $product);
		}

		$discount_amount = 0;
		foreach ($products as $key => $items) {
			//fetch discount by category
			$discount = \App\DiscountByCategory::where('date_from', '<=', $now)
				->where('date_to', '>=', $now)
				->where('category_id', $key)
				->first();
			$discount_amount_by_category = 0;
			$item_total_by_category = 0;
			//get all amount by category item
			foreach ($items as $key => $item) {
				$itemProduct = $item->product->toArray();

				$item_total_by_category = $item_total_by_category + ($itemProduct['buying_item']['item_selling_price'] * $item->quantity);
			}

			//validate discount
			if ($discount && $item_total_by_category >= $discount->min_purchase_amount) {
				if ($discount->discount_type == 'precentage') {
					$amount = ($item_total_by_category * $discount->discount) / 100;
					if ($discount->max_discount && $amount > $discount->max_discount) {
						$discount_amount += $discount->max_discount;
					} else {
						$discount_amount += $amount;
					}
				} else {
					$discount_amount += $discount->discount;
				}
			}
		}
		$discount_amount = round($discount_amount);
		$user->cart['category_discount_amount'] = $discount_amount;
		$user->cart['total_payable_amount'] = $user->cart->total_price->$discount_amount;
		return $user->cart;
	}

	public static function implementHappyHourDiscount($result) {

		$now = \Carbon\Carbon::now('Asia/Kolkata')->format('H:i');
		$day = substr(\Carbon\Carbon::now('Asia/Kolkata')->format('l'), 0, 3);
		$user = JWTAuth::user();
		if ($user && $user->user) {
			$user = $user->user;
		}
		$products = [];

		$query = 'find_in_set("' . $day . '" , on_days)';
		$discount_amount = 0;
		//fetch discount by happy hour
		$discount = \App\DiscountHappyHour::where('time_from', '<=', $now)
			->where('time_to', '>=', $now)
			->whereRaw($query)
			->first();

		if ($discount && $result->total_payable_amount > $discount->min_purchase_amount) {
			if ($discount->discount_type == 'precentage') {
				$amount = ($result->total_payable_amount * $discount->discount) / 100;
				if ($discount->max_discount && $amount > $discount->max_discount) {
					$discount_amount += $discount->max_discount;
				} else {
					$discount_amount += $amount;
				}
			} else {
				$discount_amount += $discount->discount;
			}
		}

		$discount_amount = round($discount_amount);
		$user->cart['happy_hour_discount_amount'] = $discount_amount;
		$user->cart['total_payable_amount'] = $result->total_payable_amount - $discount_amount;
		return $user->cart;
	}

	/*public static function generateInvoiceId($type = 'sell', $invoice_id = null) {
		$invoice_init_prefix = [
			'sell' => '',
			'due_paid' => 'F',
			'expense' => 'E',
		];
		$user = \JWTAuth::user();
		if ($user && $user->user) {
			$user = $user->user;
		}
		$storeId = $user->store->store_id;
		// $invoice_model = $registry->get('loader')->model('invoice'); orderBy('id', 'Desc')
		if (!$invoice_id) {
			$last_invoice = \App\SellingInfo::orderBy('info_id', 'Desc')->value('invoice_id'); //->value('invoice_id');
			$invoice_id = $last_invoice ?: $invoice_init_prefix[$type] . date('y') . date('m') . date('d') . '1';
		}
		$invId = \App\SellingInfo::where('invoice_id', $invoice_id)->first();
		// dd($invId);
		if ($invId) {
			$invoice_id = str_replace(array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'G', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'), '', $invoice_id);
			$invoice_id = (int) substr($invoice_id, -4) + 1;
			$temp_invoice_id = $invoice_init_prefix[$type] . date('y') . date('m') . date('d') . $invoice_id;
			$zero_length = 11 - strlen($temp_invoice_id);
			$zeros = '';
			for ($i = 0; $i < $zero_length; $i++) {
				$zeros .= '0';
			}
			$invoice_id = $invoice_init_prefix[$type] . date('y') . date('m') . date('d') . $storeId . $zeros . $invoice_id;
			self::generateInvoiceId($type, $invoice_id);
		} else {
			$zero_length = 11 - strlen($invoice_id);
			$zeros = '';
			for ($i = 0; $i < $zero_length; $i++) {
				$zeros .= '0';
			}
			$invoice_id = $invoice_init_prefix[$type] . date('y') . date('m') . date('d') . $storeId . $zeros . '1';
		}
		return $invoice_id;
	}*/

	public static function generateInvoiceId($type = 'sell') {
		$invoice_init_prefix = [
			'sell' => '',
			'due_paid' => 'F',
			'expense' => 'E',
		];
		$user = \JWTAuth::user();
		if ($user && $user->user) {
			$user = $user->user;
		}
		$storeId = $user->store->store_id;
		$invoice_id = $invoice_init_prefix[$type] . $storeId . $user->id . time();
		$invId = \App\SellingInfo::where('invoice_id', $invoice_id)->first();
		if ($invId) {
			self::generateInvoiceId($type);
		}
		return $invoice_id;
	}

	public static function getProductSupplier($productId) {
		$buyInfo = \DB::table('buying_info as binf')->select('binf.sup_id')
			->join('buying_item as bitm', 'bitm.invoice_id', '=', 'binf.invoice_id')
			->join('products as prd', 'prd.p_id', '=', 'bitm.item_id')
			->where('prd.p_id', $productId)
			->where('bitm.status', 'active')
			->first();
		if ($buyInfo) {
			return $buyInfo->sup_id;
		}
		return null;
	}

	public static function getProductBuyItem($productId, $storeId, $status = 'active') {
		return $buyItem = \App\BuyingItem::where([
			'store_id' => $storeId,
			'item_id' => $productId,
			'status' => $status,
		])
		// ->whereRaw('`item_quantity` > `total_sell` + `defected_quantity`')
			->first();
	}

	public static function getInvoiceInfo($invoiceId, $storeId) {
		$invInfo = \App\SellingInfo::selectRaw('selling_info.*, sp.*, c.customer_id, c.customer_name, c.customer_mobile, c.customer_email')
			->leftJoin('selling_price as sp', 'selling_info.invoice_id', '=', 'sp.invoice_id')
			->leftJoin('customers as c', 'c.customer_id', 'selling_info.customer_id')
			->where([
				// 'selling_info.store_id' => $storeId,
				'selling_info.invoice_id' => $invoiceId,
				'selling_info.inv_type' => 'sell',
			])
			// ->orderBy('selling_info.invoice_id', 'DESC')
			->first();
		return $invInfo;
	}

	public static function getInvoiceItems($invoiceId) {
		return \App\SellingItem::where('invoice_id', $invoiceId)->get();
	}

	public static function getUniqueNotifId($length = 16) {
		$token = str_random($length);
		// Check if token already exists
		if (\App\P2P_Notif::where('notif_id', $token)->exists()) {
			self::getUniqueNotifId($length);
		}
		\App\P2P_Notif::create([
			'notif_id' => $token,
		]);
		return $token;
    }

    // Upload invoice to S3 server and generate short url.
    public static function shortInvoiceUrlS3($invoiceId)
    {
        $user = \JWTAuth::user();
        $clientUser = $user->client;
        if ($user && $user->user) {
            $user = $user->user;
        }
        $accountKey = ($clientUser && $clientUser->messaging_key) ? $clientUser->messaging_key : null;
        if (!$accountKey) {
            return [
                'Success' => '0',
                'message_text' => [
                    'message' => 'Invalid associated account key',
				],
			];
        }
        $invInfo = \App\SellingInfo::where('invoice_id', $invoiceId)->first();
        $url = '';
        if ($invInfo && $invInfo->invoice_url) {
            $url = $invInfo->invoice_url;
        } else {
            $url = \App\Helpers\InvoiceHtml::generateInvoice($invoiceId);
        }
        if (!$url) {
            return false;
        }

        $shortUrl = \App\SellingInfo::where('invoice_id', $invoiceId)
            ->whereNotNull('short_url')->first();
        if ($shortUrl && $shortUrl->short_url) {
            return [
                'Success' => '1',
                'message_text' => [
                    'short_url' => $shortUrl->short_url,
                ],
            ];
        }

        $client = new \GuzzleHttp\Client(['verify' => false]);
        $shortParam['scTasks_req'] = [
            [
                'scTasksInfo' => [
                    'task_data' => [
                        [
                            'vdata' => [
                                'urlid' => 0,
                                'lurl' => $url,
                                'sdom' => 'http://qk.vu',
                                'cscd' => $user->id . $user->store->store_id .$invoiceId ,
                                'has_srdt' => 0,
                                'is_actv' => 1,
                            ],
                        ],
                    ],
                ],
            ],
            [
                'misc' => [
                    'a_ky' => $accountKey,
                    'request_source_id' => 1,
                ],
            ],
        ];
        $shortResp = $client->request('POST', env('SHORT_ENDPOINT'), [
            'form_params' => ['params' => json_encode($shortParam)],
        ]);
        $statusCode = $shortResp->getStatusCode();
        $sc = json_decode($shortResp->getBody(), true);
        if ($statusCode == 200) {
            $scInfo = $sc[0]['scTasks_resp'][0]['messageInfo'];
            if ($scInfo['Success'] == '1' && $scInfo['message_text']['short_url']) {
                $sellInfo = \App\SellingInfo::where('invoice_id', $invoiceId)->first();
                $sellInfo->short_url = $scInfo['message_text']['short_url'];
                $sellInfo->save();
            }
            return $scInfo;
        }
        return null;
	}

	public static function shortInvoiceUrl($invoiceId) {
		$user = \JWTAuth::user();
		$clientUser = $user->client;
		if ($user && $user->user) {
			$user = $user->user;
		}
		$accountKey = ($clientUser && $clientUser->messaging_key) ? $clientUser->messaging_key : null;
		if (!$accountKey) {
            return [
                'Success' => '0',
                'message_text' => [
                    'message' => 'Invalid associated account key',
				],
			];
		}
		$url = env('STORE_URL', url('/')) . 'invoice.php?invoice=' . $invoiceId;

		$shortUrl = \App\SellingInfo::where('invoice_id', $invoiceId)
			->whereNotNull('short_url')->first();
		if ($shortUrl && $shortUrl->short_url) {
			return [
				'Success' => '1',
				'message_text' => [
					'short_url' => $shortUrl->short_url,
				],
			];
		}

		$client = new \GuzzleHttp\Client(['verify' => false]);
		$shortParam['scTasks_req'] = [
			[
				'scTasksInfo' => [
					'task_data' => [
						[
							'vdata' => [
								'urlid' => 0,
								'lurl' => $url,
								'sdom' => 'http://qk.vu',
								'cscd' => $invoiceId,
								'has_srdt' => 0,
								'is_actv' => 1,
							],
						],
					],
				],
			],
			[
				'misc' => [
					'a_ky' => $accountKey,
					'request_source_id' => 1,
				],
			],
		];
		$shortResp = $client->request('POST', env('SHORT_ENDPOINT'), [
			'form_params' => ['params' => json_encode($shortParam)],
		]);
		$statusCode = $shortResp->getStatusCode();
		$sc = json_decode($shortResp->getBody(), true);
		if ($statusCode == 200) {
			$scInfo = $sc[0]['scTasks_resp'][0]['messageInfo'];
			if ($scInfo['Success'] == '1' && $scInfo['message_text']['short_url']) {
				$sellInfo = \App\SellingInfo::where('invoice_id', $invoiceId)->first();
				$sellInfo->short_url = $scInfo['message_text']['short_url'];
				$sellInfo->save();
			}
			return $scInfo;
		}
		return null;
	}

	public static function getInvoiceMessage($name, $url = false) {
        if ( $url ) {
            return "Hi {$name},\r\nClick on below link to view/download your invoice. \r\n" . $url;
        } else {
            return "Hi {$name},\r\nPlease find the invoice for your recent order.";
        }

	}
}
