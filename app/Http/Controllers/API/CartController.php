<?php

namespace App\Http\Controllers\API;

use App\Cart;
use App\CartProduct;
use App\Http\Controllers\Controller;
use App\Payment;
use App\Product;
use Illuminate\Http\Request;
use JWTAuth;

class CartController extends Controller
{
    public function index($addCart = false)
    {
        $user = JWTAuth::user();
        if ($user && $user->user) {
            $user = $user->user;
        }
        $cart = Cart::where(['user_id' => $user->id])->first();
        if ($addCart) {
            return $cart;
        }
        /* if ($cart->total_price < 0) {
            return response()->json(['status' => false, 'message' => 'After discount price TOO LOW! Please re-check cart.']);
        } */
        return response()->json(['status' => true, 'message' => null, 'data' => $cart], 200);
    }

    public function addToCarts(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required_without:barcode',
            'barcode' => 'required_without:product_id',
        ]);
        $user = JWTAuth::user();
        if ($user && $user->user) {
            $user = $user->user;
        }
        if ($user) {
            if ($request->barcode) {
                $product_id = \App\Product::where('p_code', $request->barcode)->value('p_id');
            } else {
                $product_id = $request->product_id;
            }
            $user_id = $user->id;
            $quantity = $request->quantity ?: 1;
            $cart = Cart::where(['user_id' => $user_id])->first();
            if (!$cart) {
                $cart = new Cart();
                $cart->user_id = $user_id;
                $cart->save();
            }
            /* $prod = \App\Product::find($product_id);
            return $prod->price_after_disc;
            if ($prod->price_after_disc < 0) {
                CartProduct::where('product_id', $product_id)->delete();
                return response()->json(['status' => false, 'message' => 'After discount price TOO LOW! Please re-check cart.']);
            } */

            $prd = \App\CartProduct::where('product_id', $product_id)->first();
            if ($prd) {
                $prd->quantity += $quantity;
                $prd->cart_id = $cart->cart_id;
            } else {
                $prd = new \App\CartProduct();
                $prd->product_id = $product_id;
                $prd->quantity = $quantity;
                $prd->cart_id = $cart->cart_id;
            }

            $cartProductsQuantity = CartProduct::where(['product_id' => $product_id, 'cart_id' => $cart->cart_id])->first();
            $product = Product::find($product_id);
            if ($cart->total_price < 0) {
                CartProduct::where('product_id', $product_id)->delete();
                return response()->json(['status' => false, 'message' => 'After discount price TOO LOW! Please re-check cart.']);
            }
            if (!$cartProductsQuantity) {
                $previousQuantity = 0;
            } else {
                $previousQuantity = $cartProductsQuantity->quantity;
            }
            if ($product && (int) $product->avl_quantity && ($product->avl_quantity >= ($previousQuantity + $quantity))) {
                if ($prd->save()) {
                    $cart = $this->index('addToCarts');
                    return response()->json(['status' => true, 'message' => 'Product added successfully.', 'data' => $cart], 200);
                } else {
                    return response()->json(['status' => false, 'messages' => 'Sorry! Something went wrong.'], 422);
                }
            } else {
                return response()->json(['status' => false, 'messages' => 'This product doesn\'t have enough quantity for this order.'], 422);
            }
        } else {
            return response()->json(['status' => false, 'messages' => ['You need to login first.']], 401);
        }
    }

    public function decreaseCart(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required',
        ]);
        $user = JWTAuth::user();
        if ($user && $user->user) {
            $user = $user->user;
        }
        if ($user) {
            $product_id = $request->product_id;
            $user_id = $user->id;
            $quantity = 1;
            $cart = Cart::where(['user_id' => $user_id])->first();
            if (!$cart) {
                return response()->json(['status' => false, 'messages' => 'You have not added any product in your cart.'], 422);
            }
            $prd = \App\CartProduct::where('product_id', $product_id)->first();
            if ($prd) {
                if ($prd->quantity > 1) {
                    $prd->quantity -= $quantity;
                    if ($prd->save()) {
                        return response()->json(['status' => true, 'message' => 'Success', 'data' => $cart], 200);
                    } else {
                        return response()->json(['status' => false, 'messages' => 'Sorry! Some problem occured.'], 400);
                    }
                } else {
                    if ($prd->delete()) {
                        return response()->json(['status' => true, 'message' => 'Product removed', 'data' => $cart], 200);
                    } else {
                        return response()->json(['status' => false, 'messages' => 'Sorry! Some problem occured.'], 400);
                    }
                }
            } else {
                return response()->json(['status' => false, 'messages' => 'This product is not in your cart.'], 422);
            }
        }
    }

    public function getPayableAmount(Request $request)
    {
        $result = \App\Helpers\Util::getCurrentOrderPayablePrice($request);
        $payments = Payment::get();
        $result['payment_methods'] = $payments;
        //$data = $this->calculateTax($result);
        //$result['products'] = $data;
        return response()->json(['status' => true, 'message' => 'Success', 'data' => $result], 200);
    }

    public function calculateTax($data)
    {
        $carts = [];
        foreach ($data->products as $key => $object) {
            $product = $object->product->toArray();
            $buying_item = $product['buying_item'];
            $taxAmount = ($object->quantity * $buying_item['item_selling_price']) / 100;
            $object['tax_amount'] = round($taxAmount);
            array_push($carts, $object);
        }
        return $carts;
    }

    public function removeCart(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|integer',
        ]);
        $user = JWTAuth::user();
        if ($user && $user->user) {
            $user = $user->user;
        }
        $cart = Cart::where(['user_id' => $user->id])->first();
        if ($cart) {
            $cart->products()->where('product_id', $request->product_id)->delete();
        }
        return response()->json(['status' => true, 'message' => 'Product deleted from cart.'], 200);
    }

    public function emptyCart(Request $request)
    {
        $user = JWTAuth::user();
        if ($user && $user->user) {
            $user = $user->user;
        }
        $cart = Cart::where(['user_id' => $user->id])->first();
        if ($cart) {
            $cart->products()->delete();
        }
        return response()->json(['status' => true, 'message' => 'Products deleted from cart.'], 200);
    }
}
