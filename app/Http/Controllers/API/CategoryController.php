<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;

class CategoryController extends Controller {

	public $successStatus = 200;
	/**
	 * login api
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function index(Request $request) {

		$user = JWTAuth::user();
		$store = \DB::table('user_to_store')->where('user_id', $user->user_id)->first();

		$category_ids = \DB::table('category_to_store')
			->where('store_id', $store->store_id)->pluck('ccategory_id')->toArray();

		$category = Category::where('parent_id', 0)->whereIn('category_id', $category_ids)->get();
		return response()->json(['status' => true, 'data' => $category], 200);
	}

	public function add(Request $request) {

	}
}
