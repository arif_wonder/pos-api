<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;

class CheckoutController extends Controller
{
    public function applyCoupon(Request $request)
    {
        $this->validate($request, [
            'customer_id' => 'required|integer',
            'code' => 'required|alpha_num',
        ], [
            'code.alpha_num' => 'Invalid coupon code.',
        ]);

        $user = JWTAuth::user();
        if ($user && $user->user) {
            $user = $user->user;
        }
        // \DB::enableQueryLog();
        $cpn = \App\Coupon::where('code', $request->code)
            ->where(function ($query) {
                $query->where('expired', 0)
                    ->where('valid_till', '>=', \Carbon\Carbon::now()->toDateTimeString());
            })->first();
        // dd(\DB::getQueryLog());

        $updatedPrice = $oldPrice = $user->cart->total_price;
        if ($cpn) {
            $custCpn = \App\CouponCustomer::where([
                'coupon_id' => $cpn->id,
                'customer_id' => $request->customer_id,
            ])->first();
            if (($custCpn && $cpn->limit_per_user) && ($custCpn->used_times >= (int) $cpn->limit_per_user)) {
                return response()->json(['status' => false, 'message' => 'Coupon use limit exceeded.'], 422);
            }
            if ($oldPrice >= $cpn->min_purchase_amount) {
                $discAmount = $disc = $cpn->discount;
                $type = $cpn->discount_type;
                if ($type == 'fixed') {
                    $updatedPrice -= $disc;
                } elseif ($type == 'precentage') {
                    $discAmount = ($oldPrice * $disc) / 100;
                    $discAmount = min($discAmount, $cpn->max_discount);
                    $updatedPrice -= $discAmount;
                }
                return response()->json([
                    'status' => true,
                    'data' => [
                        'original_price' => $oldPrice,
                        'updated_price' => $updatedPrice,
                        'discount' => $disc,
                        'discount_type' => $type,
                        'total_discount' => $discAmount,
                    ],
                ], 200);
            } else {
                return response()->json(['status' => false, 'message' => 'Your order value doesn\'t match with minimum purchase amount to apply this coupon.'], 422);
            }

        }
        return response()->json(['status' => false, 'message' => 'Invalid coupon.'], 400);
    }

    public function placeOrder(Request $request)
    {
        $this->validate($request, [
            'customer_id' => 'required|integer',
            'payment_method_id' => 'required|integer',
            'paid_amount' => 'required|numeric',
            // 'payment_mode' => 'required|alpha',
            'ref_no' => 'required|exists:p2p_notif,notif_id',
        ], [
            'payment_method_id.required' => 'Payment method is required.',
            'payment_method_id.integer' => 'Payment method must be an integer.',
            'ref_no.exists' => 'Payment id is not valid.',
        ]);
        // dd($request->use_credit);

        // Check if notification id is already used
        if (\App\SellingPrice::where('payment_id', $request->ref_no)->exists()) {
            return response()->json(['status' => false, 'message' => 'This payment id is already used with another transaction.'], 422);
        }

        $user = JWTAuth::user();
        if ($user && $user->user) {
            $user = $user->user;
        }
        $storeId = $user->store->store_id;
        $customer = \App\Customer::find($request->customer_id);
        // $dueAmount = $customer->stores()->where('store_id', $storeId)->value('due_amount');
        $cartData = $user->cart;
        if (!$cartData || !$cartData->products()->count()) {
            return response()->json(['status' => false, 'message' => 'Please add product(s) first.'], 422);
        }
        $useCredit = ($request->use_credit === 'true' || $request->use_credit === true) ? true : false;
        // return response()->json(['$useCredit' => $useCredit]);

        $productDiscount = $cartData->total_discount;
        $perPrdDiscount = 0;
        $payableAmount = $cartData->total_price;
        $prdCount = $cartData->products->count();

        // Apply coupon on order
        if ($request->coupon_code) {
            $cpn = \App\Coupon::where('code', $request->coupon_code)
                ->where(function ($query) {
                    $query->where('expired', 0)
                        ->where('valid_till', '>=', \Carbon\Carbon::now()->toDateTimeString());
                })->first();
            if ($cpn) {
                $custCpn = \App\CouponCustomer::where([
                    'coupon_id' => $cpn->id,
                    'customer_id' => $request->customer_id,
                ])->first();
                if ($custCpn) {
                    if ($cpn->limit_per_user && ($custCpn->used_times >= (int) $cpn->limit_per_user)) {
                        return response()->json(['status' => false, 'message' => 'Coupon use limit exceeded.'], 422);
                    }

                    if ($payableAmount < $cpn->min_purchase_amount) {
                        return response()->json(['status' => false, 'message' => 'Your order value doesn\'t match with minimum purchase amount to apply this coupon.'], 422);
                    }
                    $discAmount = $disc = $cpn->discount;
                    $type = $cpn->discount_type;
                    if ($type == 'precentage') {
                        $discAmount = ($payableAmount * $disc) / 100;
                        $discAmount = min($discAmount, $cpn->max_discount);
                    }
                    $productDiscount += $discAmount;
                    $payableAmount -= $discAmount;
                    $perPrdDiscount = $discAmount / $prdCount;
                    $custCpn->increment('used_times');
                } else {
                    $discAmount = $disc = $cpn->discount;
                    $type = $cpn->discount_type;
                    if ($type == 'precentage') {
                        $discAmount = ($payableAmount * $disc) / 100;
                        $discAmount = min($discAmount, $cpn->max_discount);
                    }
                    $productDiscount += $discAmount;
                    $payableAmount -= $discAmount;
                    $perPrdDiscount = $discAmount / $prdCount;
                    \App\CouponCustomer::create([
                        'coupon_id' => $cpn->id,
                        'customer_id' => $request->customer_id,
                        'used_times' => 1,
                    ]);
                }
            }
        }
        // return [$productDiscount, $payableAmount];
        $todaysAmount = $payableAmount;
        $subtotal = $payableAmount + $productDiscount;

        // $customer = \App\Customer::find($request->customer_id);
        $creditAmount = 0;
        $paidAmount = $request->paid_amount;
        if ($useCredit && (int) $customer->credit_balance > 0) {
            if ($payableAmount >= $customer->credit_balance) {
                $creditAmount = $customer->credit_balance;
            } else {
                // $creditAmount = $customer->credit_balance - $payableAmount;
                $creditAmount = $payableAmount;
            }
            // return [$creditAmount, $payableAmount];
            $customer->decrement('credit_balance', $creditAmount);
            $paidAmount = $paidAmount + $creditAmount;
        }
        $prevDue = $customer->stores()->where('store_id', $storeId)->value('due_amount');
        $todayDue = $payableAmount > $paidAmount ? $payableAmount - $paidAmount : 0;

        $todaysDue = $todaysAmount > $paidAmount ? $todaysAmount - $paidAmount : 0;
        $duePaidAmount = $paidAmount > $todaysAmount ? $paidAmount - $todaysAmount : 0;
        //return $presentDues = abs(($todaysAmount + $prevDue) - $paidAmount);
        // return $presentDues = ($prevDue + $todayDue);
        $presentDues = ($prevDue + $todayDue) - $duePaidAmount;

        // return $productDiscount;
        $productTax = $cartData->total_tax;

        $isPaid = $todaysDue ? 0 : 1;
        $isDue = !$isPaid ? 1 : 0;

        // Generate unique invoice id
        $invoiceId = \App\Helpers\Util::generateInvoiceId('sell');
        $paymentMethod = \App\Payment::find($request->payment_method_id);
        // Insert data info selling_info table
        $sellingInfo = \App\SellingInfo::create([
            'invoice_id' => $invoiceId,
            'store_id' => $storeId,
            'customer_id' => $request->customer_id,
            'currency_code' => 'INR',
            'payment_method' => $request->payment_method_id,
            'payment_mode' => $paymentMethod ? $paymentMethod->name : null,
            'created_by' => $user->id,
            'invoice_note' => $request->invoice_note,
            'cart_data' => $cartData,
            'is_paid' => $isPaid,
            'is_due' => $isDue,
        ]);

        // Insert data info selling_price table
        $sellingPrice = \App\SellingPrice::create([
            'invoice_id' => $invoiceId,
            'store_id' => $storeId,
            'subtotal' => $subtotal,
            'discount_amount' => $productDiscount,
            'tax_amount' => $cartData->total_tax,
            'previous_due' => $prevDue,
            'payable_amount' => $payableAmount,
            'paid_amount' => $paidAmount,
            'todays_due' => round($todaysDue, 2),
            'present_due' => round($presentDues, 2),
            'coupon_id' => ($request->coupon_code && $cpn) ? $cpn->id : null,
            'paid_by_credit' => $useCredit ? round($creditAmount, 2) : 0,
            'payment_id' => $request->ref_no,
        ]);

        // Update customer balance
        $custStore = \App\StoreCustomer::where([
            'customer_id' => $request->customer_id,
            'store_id' => $storeId,
        ])->first();
        if ($custStore) {
            $custStore->due_amount = round($presentDues, $presentDues);
            $custStore->currency_code = 'INR';
            $custStore->save();
        }
        // Loop through the invoice product items for storing info. into selling_item table
        // Adjustment buying invoice information
        foreach ($cartData->products as $product) {
            $totalBuyPrice = 0;
            for ($i = 0; $i < $product->quantity; $i++) {
                $buyItem = \App\Helpers\Util::getProductBuyItem($product->product_id, $storeId);
                if (!$buyItem) {
                    $buyItem = \App\Helpers\Util::getProductBuyItem($product->product_id, $storeId, 'stock');
                    if ($buyItem) {
                        // Activate another stock item
                        $buyItem->status = 'active';
                        $buyItem->save();
                    }
                }
                $supId = \App\Helpers\Util::getProductSupplier($product->product_id);

                if ($buyItem) {
                    $buyInvId = $buyItem->invoice_id;
                    // Increment item valude in buying item table
                    $buyItem->total_sell += 1;
                    $buyItem->save();

                    $totalBuyPrice += $buyItem->item_buying_price;

                    if ($buyItem->item_quantity <= ($buyItem->total_sell + $buyItem->defected_quantity)) {
                        // increment item valude in buying item table
                        $buyItem->status = 'sold';
                        $buyItem->save();

                        // Fetch another stock item
                        $buyItem = \App\Helpers\Util::getProductBuyItem($product->product_id, $storeId, 'stock');

                        if ($buyItem) {
                            // Activate another stock item
                            $buyItem->status = 'active';
                            $buyItem->save();
                        }
                    }
                } else {
                    return response()->json(['status' => false, 'message' => 'Product not found.'], 422);
                }
            }
            // Buying price adjustment end

            // Insert product into selling_item table
            $sellingItem = \App\SellingItem::create([
                'invoice_id' => $invoiceId,
                'store_id' => $storeId,
                'item_id' => $product->product_id,
                'category_id' => $product->product->category_id,
                'sup_id' => $supId,
                'item_name' => $product->product->p_name,
                'total_buying_price' => $totalBuyPrice,
                'item_price' => $product->product->selling_price,
                'item_discount' => $product->discount,
                'item_tax' => $product->tax,
                'item_quantity' => $product->quantity,
                'item_total' => $product->total_price,
                'buying_invoice_id' => $buyInvId,
            ]);

            // decrease product quantity
            \DB::table('product_to_store')->where([
                'store_id' => $storeId,
                'product_id' => $product->product_id,
            ])
                ->decrement('quantity_in_stock', $product->quantity);
        }

        // Create due paid invoice
        if ($duePaidAmount > 0) {
            $paidInvId = \App\Helpers\Util::generateInvoiceId('due_paid');
            $datetime = date('Y-m-d H:i:s');
            $presentDue = (float) ($prevDue - $duePaidAmount);

            $dsInfo = \App\SellingInfo::create([
                'invoice_id' => $paidInvId,
                'store_id' => $storeId,
                'inv_type' => 'due_paid',
                'customer_id' => $request->customer_id,
                'currency_code' => 'INR',
                'payment_method' => $request->payment_method_id,
                'ref_invoice_id' => $invoiceId,
                'created_by' => $user->id,
            ]);
            $dsPrice = \App\SellingPrice::create([
                'invoice_id' => $paidInvId,
                'store_id' => $storeId,
                'previous_due' => round($prevDue, 2),
                'paid_amount' => round($duePaidAmount, 2),
                'todays_due' => round($todaysDue, 2),
                'present_due' => round($presentDues, 2),
            ]);
            $dsItem = \App\SellingItem::create([
                'invoice_id' => $paidInvId,
                'store_id' => $storeId,
                'item_id' => 0,
                'item_name' => 'Deposit',
                'item_price' => round($duePaidAmount, 2),
                'item_quantity' => 1,
                'item_total' => round($duePaidAmount, 2),
            ]);
        }
        $cartData->products()->delete();

        $invoiceInfo = \App\Helpers\Util::getInvoiceInfo($invoiceId, $storeId);
        $invoiceItems = \App\Helpers\Util::getInvoiceItems($invoiceId);

        return response()->json(['status' => true, 'data' => [
            'invoice_id' => $invoiceId,
            'invoice_info' => $invoiceInfo,
            'invoice_items' => $invoiceItems,
        ]], 200);
    }
}
