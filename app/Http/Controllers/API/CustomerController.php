<?php

namespace App\Http\Controllers\API;

use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;

class CustomerController extends Controller {

	public $successStatus = 200;
	/**
	 * Get Customer
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function index(Request $request) {
		$user = JWTAuth::user();
		if ($user && $user->user) {
			$user = $user->user;
		}
		$storeId = $user->store->store_id;
		$custQuery = Customer::join('customer_to_store as c2s', function ($join) use ($storeId) {
			$join->on('customers.customer_id', '=', 'c2s.customer_id')
				->where('c2s.store_id', '=', $storeId);
		});
		if ($request->search) {
			$custQuery->where(function ($query) use ($request) {
				$query->where('customer_name', 'like', '%' . $request->search . '%');
				$query->orWhere('customer_email', 'like', '%' . $request->search . '%');
				$query->orWhere('customer_mobile', 'like', '%' . $request->search . '%');
			});
		}
		$customer = $custQuery->get();
		return response()->json(['status' => true, 'data' => $customer], 200);
	}

	public function create(Request $request) {
		$this->validate($request, [
			//'customer_name' => 'required|string|max:50',
			'customer_email' => 'nullable|string|email',
			'customer_mobile' => 'required|digits:10',
		]);

		$user = JWTAuth::user();
		if ($user && $user->user) {
			$user = $user->user;
		}
        $storeId = $user->store->store_id;
        // \DB::enableQueryLog();
        $cust = Customer::where(function ($query) use ($request) {
                    if ($request->customer_email) {
                        $query->where('customer_email', $request->customer_email);
                        if ($request->customer_mobile) {
                            $query->orWhere('customer_mobile', $request->customer_mobile);
                        }
                    } else {
                        if ($request->customer_mobile) {
                            $query->where('customer_mobile', $request->customer_mobile);
                        }
                    }
                })->join('customer_to_store as c2s', function ($join) use ($storeId) {
                    $join->on('customers.customer_id', '=', 'c2s.customer_id')
                        ->where('c2s.store_id', '=', $storeId);
                })->count();
        // dump($cust);
        // dd(\DB::getQueryLog());
        if ($cust) {
            return response()->json(['status' => false, 'message' => 'Customer already exists.'], 422);
        }
		$customer = Customer::create([
			'customer_name' => $request->customer_name,
			'customer_email' => $request->customer_email,
			'customer_mobile' => $request->customer_mobile,
			'customer_sex' => $request->customer_sex,
			'customer_age' => $request->customer_age,
			'customer_address' => $request->customer_address,
			'anniversary_date' => $request->anniversary_date,
			'occupation' => $request->occupation,
			'dob' => $request->dob,
			'marital_status' => $request->marital_status,
			'no_of_children' => $request->no_of_children,
			'alt_contact_number' => $request->alt_contact_number,
			'passport_number' => $request->passport_number,
			'dl_number' => $request->dl_number,
			'pan_number' => $request->pan_number,
			'aadhar_number' => $request->aadhar_number,
		]);
		// dd($customer);
		if ($customer) {
			$customer->stores()->create([
				'store_id' => $user->store->store_id,
				'currency_code' => 'INR',
				'status' => 1,
			]);
			return response()->json(['status' => true, 'messages' => 'Customer created successfully', 'data' => $customer], 200);
		} else {
			return response()->json(['status' => false, 'messages' => 'There is something wrong.'], 400);
		}
	}
}
