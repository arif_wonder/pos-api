<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function index(Request $request)
    {
        $user = \JWTAuth::user();
        if ($user && $user->user) {
            $user = $user->user;
        }
        $storeId = $user->store->store_id;
        $invQuery = \DB::table('selling_info as si')->selectRaw('si.*, sp.*, c.customer_id, c.customer_name, c.customer_mobile, c.customer_email')
            ->leftJoin('selling_price as sp', 'si.invoice_id', '=', 'sp.invoice_id')
            ->leftJoin('customers as c', 'c.customer_id', 'si.customer_id')
            ->where([
                'si.store_id' => $storeId,
                'si.inv_type' => 'sell',
            ]);
        if ($request->search) {
            $invQuery->where(function ($query) use ($request) {
                $query->orWhere('si.invoice_id', $request->search);
                $query->orWhere('c.customer_name', 'like', '%' . $request->search . '%');
                $query->orWhere('c.customer_email', 'like', '%' . $request->search . '%');
            });
        }
        if ($request->date_from) {
            $invQuery->where('si.created_at', '>=', \Carbon\Carbon::parse($request->date_from));
        }
        if ($request->date_to) {
            $invQuery->where('si.created_at', '<=', \Carbon\Carbon::parse($request->date_to)->addHours(23)->addMinutes(59)->addSeconds(59));
        }
        /* if ($request->date_from && $request->date_to) {
        $invQuery->whereBetween('si.created_at', [
        \Carbon\Carbon::parse($request->date_from)->toDateTimeString(),
        \Carbon\Carbon::parse($request->date_to)->toDateTimeString(),
        ]);
         */
        $invoices = $invQuery->orderBy('si.created_at', 'DESC')
            ->paginate($this->getPerPage());
        $invoices = $this->getPaginated($invoices);
        // $invoices = $invQuery->toSql();
        return response()->json(['status' => true, 'data' => $invoices], 200);
    }

    public function getInvoiceData(Request $request, $invid)
    {
        if (!$invid) {
            return response()->json(['status' => false, 'message' => 'Invalid invoice id'], 422);
        }
        $user = \JWTAuth::user();
        if ($user && $user->user) {
            $user = $user->user;
        }
        $storeId = $user->store->store_id;
        $invoiceId = $invid;

        $invoiceInfo = \App\Helpers\Util::getInvoiceInfo($invoiceId, $storeId);
        $invoiceItems = \App\Helpers\Util::getInvoiceItems($invoiceId);

        return response()->json(['status' => true, 'data' => [
            'invoice_id' => $invoiceId,
            'invoice_info' => $invoiceInfo,
            'invoice_items' => $invoiceItems,
        ]], 200);
    }

    public function returnItem(Request $request)
    {
        $this->validate($request, [
            'invoice_item_id' => 'required',
            'quantity' => 'required|integer',
            'defected' => 'required|in:true,false',
            'comment' => 'required_if:defected,true',
            'image' => 'required_if:defected,true',
        ]);
        $user = \JWTAuth::user();
        if ($user && $user->user) {
            $user = $user->user;
        }
        $storeId = $user->store->store_id;
        $quantity = $request->quantity;

        $selItem = \App\SellingItem::find($request->invoice_item_id);
        // dd($selItem);
        if ($selItem) {
            $selInfo = $selItem->sellingInfo;
        } else {
            return response()->json(['status' => false, 'message' => 'Product not found.'], 422);
        }

        if ($quantity > $selItem->item_quantity) {
            return response()->json(['status' => false, 'message' => 'Product return quantity can\'t be greater than purchased quantity.'], 422);
        }
        // Check if already returned.
        $returned = \App\ReturnProduct::selectRaw('COUNT(quantity) as returnedQuantity, SUM(quantity) as sum_quantity')->where([
            'info_id' => $request->invoice_item_id,
        ])->first();
        if ($returned) {
            if (($returned->returnedQuantity >= $selItem->item_quantity) || ($returned->sum_quantity >= $selItem->item_quantity)) {
                return response()->json(['status' => false, 'message' => 'This product is already returned'], 422);
            }
            $retQty = max($returned->sum_quantity, $returned->returnedQuantity);
            $remianQty = $selItem->item_quantity - $retQty;
            if ((int) $remianQty < $quantity) {
                return response()->json(['status' => false, 'message' => 'Invalid quantity supplied to return.'], 422);
            }
        }

        $imageName = '';
        if ($request->file('image')) {
            $file = $request->file('image');
            $imageName = time() . '-' . $file->getClientOriginalName();
            $destinationPath = env('FILE_UPLOAD_PATH');
            \File::isDirectory($destinationPath) or \File::makeDirectory($destinationPath, 0777, true, true);
            $file->move($destinationPath, $file->getClientOriginalName());
        }
        if ($selItem && $selInfo) {
            // Update customer credit/debit.
            $retAmount = $selItem->item_total - $selItem->item_discount;
            $amtPerProduct = $retAmount / $selItem->item_quantity;
            $retAmount = $amtPerProduct * $quantity;
            $customer = \App\Customer::find($selInfo->customer_id);
            $custStore = $customer->stores;
            $prevDue = $custStore->due_amount;
            $presentDue = $duePaidAmount = 0;
            $creditAmt = $retAmount;
            $paidInvId = '';
            // dd($prevDue);
            if ($prevDue > 0) {
                $paidInvId = \App\Helpers\Util::generateInvoiceId('due_paid');
                $datetime = date('Y-m-d H:i:s');
                if ($prevDue >= $retAmount) {
                    $presentDue = $prevDue - $retAmount;
                    $duePaidAmount = $retAmount;
                    $creditAmt = 0;
                } else if ($prevDue < $retAmount) {
                    $creditAmt = $retAmount - $prevDue;
                    $duePaidAmount = $prevDue;
                }
                $dsInfo = \App\SellingInfo::create([
                    'invoice_id' => $paidInvId,
                    'store_id' => $storeId,
                    'inv_type' => 'due_paid',
                    'customer_id' => $customer->customer_id,
                    'currency_code' => 'INR',
                    'payment_method' => '0',
                    // 'ref_invoice_id' => $invoiceId,
                    'created_by' => $user->id,
                ]);
                $dsPrice = \App\SellingPrice::create([
                    'invoice_id' => $paidInvId,
                    'store_id' => $storeId,
                    'previous_due' => $prevDue,
                    'paid_amount' => $duePaidAmount,
                    'todays_due' => 0,
                    'present_due' => $presentDue,
                ]);
                $dsItem = \App\SellingItem::create([
                    'invoice_id' => $paidInvId,
                    'store_id' => $storeId,
                    'item_id' => 0,
                    'item_name' => 'Deposit',
                    'item_price' => $duePaidAmount,
                    'item_quantity' => 1,
                    'item_total' => $duePaidAmount,
                ]);
                $custStore->due_amount = $presentDue;
                $custStore->save();
            }
            // Insert return info.
            $returnedInfo = \App\ReturnProduct::create([
                'quantity' => $quantity,
                'info_id' => $request->invoice_item_id,
                'invoice_id' => $selInfo->invoice_id,
                'product_id' => $selItem->item_id,
                'customer_id' => $selInfo->customer_id,
                'user_id' => $user->id,
                'store_id' => $user->store->store_id,
                'comments' => $request->comment,
                'image' => '/' . $imageName,
                'ret_amount' => $retAmount,
                'return_date' => now(),
            ]);
            $customer->credit_balance = $creditAmt;
            $customer->save();
            $selItem->returned = 1;
            $selItem->save();

            // Update buying item info.
            $buyItem = \App\BuyingItem::where('invoice_id', $selItem->buying_invoice_id)->first();
            // update total sell in
            $buyItem->decrement('total_sell', $quantity);
            if ($request->defected === 'true') {
                $buyItem->increment('defected_quantity', $quantity);
            } else {
                // update store product quantity.
                \DB::table('product_to_store')->where([
                    'store_id' => $user->store->store_id,
                    'product_id' => $selItem->item_id,
                ])->increment('quantity_in_stock', $quantity);
            }
            $buyItemAct = \App\BuyingItem::where('item_id', $buyItem->item_id)->where('status', 'active')->first();
            if ($buyItemAct) {
                $buyItem->status = 'stock';
            } else {
                $buyItem->status = 'active';
            }
            $buyItem->save();

            return response()->json(['status' => true, 'data' => ['returned_info' => $returnedInfo, 'due_invoice_id' => $paidInvId]], 200);
        } else {
            return response()->json(['status' => false, 'message' => 'Invalid request.'], 422);
        }

        return response()->json(['status' => false, 'message' => 'Invalid request.'], 422);
    }
}
