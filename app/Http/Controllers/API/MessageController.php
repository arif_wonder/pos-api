<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use GuzzleHttp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class MessageController extends Controller {
	public function bulkSend(Request $request) {
		$this->validate($request, [
			'customer_phones' => 'required|string',
			'message' => 'required|string',
		]);
		// dd($request->customer_phones);
		// http://smsbusiness.in/api/pushsms.php?usr=your profile id&key=your http api key&sndr=Sender id&ph=mobile1,mobile2,mobile3&text=Text message&rpt=1
		// http://push.ezysms.in/api.php?username=mobiri&password=369304&sender=MOBIRI&sendto=7210007956,9643316822&message=TestBulk
		$endpoint = env('SMS_ENDPOINT');
		$client = new GuzzleHttp\Client();
		$response = $client->request('GET', $endpoint, ['query' => [
			'username' => env('SMS_UNAME'),
			'password' => env('SMS_PASS'),
			'sender' => env('SMS_SENDER'),
			'sendto' => $request->customer_phones,
			'message' => $request->message,
		]]);
		$statusCode = $response->getStatusCode();
		$content = $response->getBody();
		if ($statusCode == 200) {
			$message = \App\BulkMessage::create([
				'customer_phones' => $request->customer_phones,
				'message' => $request->message,
			]);
			return response()->json(['status' => true, 'data' => $message], 200);
		}
		return response()->json(['status' => false, 'message' => 'Problem in sending sms. Please try again later.'], 422);

	}

	/**
	 * Send invoice by SMS
	 */
	public function sendInvoiceSMS(Request $request) {
		$this->validate($request, [
			'invoice_id' => 'required',
		]);

		$invInfo = \App\SellingInfo::where('invoice_id', $request->invoice_id)->first();
		if (!$invInfo) {
			return response()->json(['status' => false, 'message' => 'Invalid invoice id'], 422);
		}
		$scInfo = \App\Helpers\Util::shortInvoiceUrlS3($request->invoice_id);
		if (!$scInfo || $scInfo['Success'] == '0') {
			return response()->json(['status' => false, 'message' => 'Your account is not configured to send messages.'], 422);
		}
		$shortUrl = ($scInfo && $scInfo['Success'] == '1') ? $scInfo['message_text']['short_url'] : null;

		if ($shortUrl) {
			$customer = \App\Customer::find($invInfo->customer_id);
			$client = new GuzzleHttp\Client(['verify' => false]);
			$endpoint = env('SMS_ENDPOINT');
			$response = $client->request('GET', $endpoint, ['query' => [
				'username' => env('SMS_UNAME'),
				'password' => env('SMS_PASS'),
				'sender' => env('SMS_SENDER'),
				'sendto' => $customer->customer_mobile,
				'message' => \App\Helpers\Util::getInvoiceMessage($customer->customer_name, $shortUrl),
			]]);
			$statusCode = $response->getStatusCode();
			if ($statusCode == 200) {
				return response()->json(['status' => true, 'message' => 'Invoice sent successfully.', 'shorturl' => $shortUrl, 'phone' => $customer->customer_mobile], 200);
			}
		}
		return response()->json(['status' => false, 'message' => 'Problem in sending sms. Please try again later.'], 422);
	}

	/**
	 * Send P2P notification to device.
	 */
	public function sendP2PNotification(Request $request) {
		$this->validate($request, [
			'customer_id' => 'required|exists:customers,customer_id',
			'amount' => 'required',
			'payment_mode' => 'required|in:CARD,CASH',
		]);

		$user = \JWTAuth::user();
		$client = $user->client;
		if ($user && $user->user) {
			$user = $user->user;
		}
		// dd($client->toArray());
		$deviceId = $user->device_id ? $user->device_id : env('P2P_DEVICE');
		if (!$user->device_type || !$deviceId || $user->device_type != 'D200') {
			return response()->json(['status' => false, 'message' => 'Invalid payment device detail, notification couldn\'t be sent.']);
		}

		// Clear old notification ids and get new one.
		$exitCode = Artisan::call('notif:clear');
		$token = \App\Helpers\Util::getUniqueNotifId();

		$customer = \App\Customer::find($request->customer_id);
		if ($customer && $token) {
			$client = new \GuzzleHttp\Client();
			$jsonArr = [
				'appKey' => env('P2P_KEY'),
				'username' => env('P2P_USERNAME'),
				'amount' => $request->amount,
				'customerMobileNumber' => $customer->customer_mobile,
				'externalRefNumber' => $token,
				'customerEmail' => $customer->customer_email,
				// 'paymentMode' => $request->payment_mode,
				'pushTo' => [
					'deviceId' => $deviceId,
				],
			];
			$response = $client->post(env('P2P_URL') . '/start', ['json' => $jsonArr]);
			$statusCode = $response->getStatusCode();
			$content = $response->getBody();
			if ($statusCode == 200) {
				return response()->json(['status' => true, 'message' => 'P2P notification sent successfully.', 'data' => ['content' => $content, 'payment_id' => $token]], 200);
			}
			return response()->json(['status' => false, 'message' => 'Problem in sending P2P notification. Please try again later.'], 422);
		}
		return response()->json(['status' => false, 'message' => 'Customer doesn\'t exists'], 422);
	}
	/**
	 * Get unique P2P notification id to process payment.
	 */
	public function getP2PNotifId(Request $request) {
		// Clear old notification ids and get new one.
		$exitCode = Artisan::call('notif:clear');
		$token = \App\Helpers\Util::getUniqueNotifId();
		if ($token) {
			return response()->json(['status' => true, 'data' => ['payment_id' => $token]], 200);
		}
		return response()->json(['status' => false, 'message' => 'Some server error occurred, please try again later.'], 422);
	}

	/**
	 * Send invoice by WhatsApp
	 */
	public function sendInvoiceWA(Request $request) {
		$this->validate($request, [
			'invoice_id' => 'required',
		]);
		$user = \JWTAuth::user();
		$client = $user->client;
		if ($user && $user->user) {
			$user = $user->user;
		}
		$accountKey = ($client && $client->messaging_key) ? $client->messaging_key : null;
		$WANumber = ($client && $client->message_number) ? $client->message_number : null;
		$WACC = ($client && $client->message_number_cc) ? $client->message_number_cc : null;
		if (!$accountKey) {
			return response()->json(['status' => false, 'message' => 'Invalid associated account key'], 422);
		}
		$invInfo = \App\SellingInfo::where('invoice_id', $request->invoice_id)->first();
		if (!$invInfo) {
			return response()->json(['status' => false, 'message' => 'Invalid invoice id'], 422);
		}
		/* $scInfo = \App\Helpers\Util::shortInvoiceUrlS3($request->invoice_id);
        $shortUrl = ($scInfo && $scInfo['Success'] == '1') ? $scInfo['message_text']['short_url'] : null; */

		$invUrl = \App\Helpers\InvoiceHtml::generateInvoice($request->invoice_id);

		if ($invUrl) {
			$customer = \App\Customer::find($invInfo->customer_id);
			$client = new GuzzleHttp\Client(['verify' => false]);
			$customerMobileCC = $customer->mobile_cc ?: 91;
			$params['waTasks_req'] = [
				[
					'waTasksInfo' => [
						'task_id' => 2,
						'task_data' => [
							[
								'vdata' => [
									'cc' => $WACC,
									'ph' => $WANumber,
									'rcc' => $customerMobileCC,
									'rph' => $customer->customer_mobile,
									't_msg' => 1,
									"md_url" => $invUrl,
									'msg_cap' => \App\Helpers\Util::getInvoiceMessage($customer->customer_name),
								],
							],
						],
					],
				],
				[
					'misc' => [
						'a_ky' => $accountKey,
						'request_source_id' => 1,
					],
				],
			];
			$response = $client->request('POST', env('WA_ENDPOINT'), [
				'form_params' => ['params' => json_encode($params)],
			]);
			$statusCode = $response->getStatusCode();
			if ($statusCode == 200) {
				$sc = json_decode($response->getBody(), true);
				$scInfo = $sc[0]['waTasks_resp'][0]['messageInfo'];
				if ($scInfo['Success'] == '1') {
					return response()->json(['status' => true, 'message' => 'Invoice sent successfully.', 'shorturl' => $invUrl, 'phone' => $customer->customer_mobile], 200);
				}
				return response()->json(['status' => false, 'message' => 'WhatsApp Error, while sending invoice', 'error_data' => $sc], 400);
			}
		}
		return response()->json(['status' => false, 'message' => 'Problem in sending sms. Please try again later.'], 422);
	}

	public function testP2P(Request $request) {
		$token = \App\Helpers\Util::getUniqueNotifId();
		$deviceId = $request->device_id ?: env('P2P_DEVICE');
		$client = new \GuzzleHttp\Client();
		$jsonArr = [
			'appKey' => env('P2P_KEY'),
			'username' => env('P2P_USERNAME'),
			'amount' => $request->amount,
			'externalRefNumber' => $token,
			// 'paymentMode' => $request->payment_mode,
			'pushTo' => [
				'deviceId' => $deviceId,
			],
		];
		$response = $client->post(env('P2P_URL') . '/start', ['json' => $jsonArr]);
		$statusCode = $response->getStatusCode();
		$content = $response->getBody();
		if ($statusCode == 200) {
			return response()->json(['status' => true, 'message' => 'P2P notification sent successfully.', 'data' => ['content' => $content, 'payment_id' => $token]], 200);
		}
	}
}
