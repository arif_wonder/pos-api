<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OfflineOrderController extends Controller {
	/**
	 * Sync Offline Orders
	 *
	 * @param Illuminate\Http\Request
	 * @return Illuminate\Http\Response
	 */
	public function syncOrders(Request $request) {
		//dd($request->all());
		$validator = Validator::make($request->input(), [
			'customer_mobile' => 'required|digits:10',
			'customer_name' => 'required|string|max:150',
			'invoice_id' => 'required',
			'payment_method_id' => 'required|integer',
			'paid_amount' => 'required|numeric',
			'products.*.id' => 'required|integer',
			'products.*.quantity' => 'required|integer',
			'total_discount' => 'nullable|numeric',
			'total_tax' => 'nullable|numeric',
		]);
		if ($validator->fails()) {
			return response()->json(['status' => false, 'message' => $validator->messages()], 422);
        }
		$user = \JWTAuth::user();
		if ($user && $user->user) {
			$user = $user->user;
		}
		$storeId = $user->store->store_id;

		$paidAmount = $request->paid_amount;
		$productDiscount = $request->total_discount;
		$productTax = $request->total_tax;
		$invoiceId = $request->invoice_id;
		$subtotal = $paidAmount + $productDiscount;

		$prdCount = count($request->products);
		$perPrdDiscount = $productDiscount / $prdCount;
		$perPrdTax = $productTax / $prdCount;

		$paymentMethod = \App\Payment::find($request->payment_method_id);
		$customer = \App\Customer::where('customer_mobile', $request->customer_mobile)->first();
		if (!$customer) {
			$customer = \App\Customer::create([
				'customer_name' => $request->customer_name,
				'customer_mobile' => $request->customer_mobile,
			]);
			$storeCust = \App\StoreCustomer::create([
				'customer_id' => $customer->customer_id,
				'store_id' => $storeId,
				'currency_code' => 'INR',
			]);
		}
		$prevDue = $customer->stores()->where('store_id', $storeId)->value('due_amount');

		// Insert data info selling_info table
		$sellingInfo = \App\SellingInfo::create([
			'invoice_id' => $invoiceId,
			'store_id' => $storeId,
			'customer_id' => $customer->customer_id,
			'currency_code' => 'INR',
			'payment_method' => $request->payment_method_id,
			'payment_mode' => $paymentMethod ? $paymentMethod->name : null,
			'created_by' => $user->id,
			'invoice_note' => $request->invoice_note,
			'cart_data' => null,
			'is_paid' => 1,
			'is_due' => 0,
		]);

		// Insert data info selling_price table
		$sellingPrice = \App\SellingPrice::create([
			'invoice_id' => $invoiceId,
			'store_id' => $storeId,
			'subtotal' => $subtotal,
			'discount_amount' => $productDiscount,
			'tax_amount' => $request->total_tax,
			'previous_due' => $prevDue,
			'payable_amount' => $paidAmount,
			'paid_amount' => $paidAmount,
			'todays_due' => 0,
			'present_due' => $prevDue,
			'coupon_id' => null,
			'paid_by_credit' => 0,
			'payment_id' => null,
		]);

		// Loop through the invoice product items for storing info. into selling_item table
		// Adjustment buying invoice information
		foreach ($request->products as $product) {
			$supId = \App\Helpers\Util::getProductSupplier($product['id']);
			$prodDtl = \App\Product::find($product['id']);
			if (!$prodDtl) {
				return response()->json(['status' => false, 'message' => 'Product not found']);
			}
			$totalBuyPrice = 0;

			for ($i = 0; $i < $product['quantity']; $i++) {
				$buyItem = \App\Helpers\Util::getProductBuyItem($product['id'], $storeId);
				if ($buyItem) {
					$buyInvId = $buyItem->invoice_id;
					// Increment item valude in buying item table
					$buyItem->total_sell += 1;
					$buyItem->save();

					$totalBuyPrice += $buyItem->item_buying_price;

					if ($buyItem->item_quantity <= ($buyItem->total_sell + $buyItem->defected_quantity)) {
						// increment item valude in buying item table
						$buyItem->status = 'sold';
						$buyItem->save();

						// Fetch another stock item
						$buyItem = \App\Helpers\Util::getProductBuyItem($product['id'], $storeId, 'stock');

						if ($buyItem) {
							// Activate another stock item
							$buyItem->status = 'active';
							$buyItem->save();
						}
					}
				}
			} // Buying price adjustment end
			// Insert product into selling_item table
			$sellingItem = \App\SellingItem::create([
				'invoice_id' => $invoiceId,
				'store_id' => $storeId,
				'item_id' => $prodDtl->p_id,
				'category_id' => $prodDtl->category_id,
				'sup_id' => $supId,
				'item_name' => $prodDtl->p_name,
				'total_buying_price' => $totalBuyPrice,
				'item_price' => $prodDtl->selling_price,
				'item_discount' => $perPrdDiscount,
				'item_tax' => $perPrdTax,
				'item_quantity' => $product['quantity'],
				'item_total' => $prodDtl->selling_price * $product['quantity'],
				'buying_invoice_id' => $buyInvId,
			]);

			// decrease product quantity
			\DB::table('product_to_store')->where([
				'store_id' => $storeId,
				'product_id' => $product['id'],
			])->decrement('quantity_in_stock', $product['quantity']);
		}

		return response()->json(['status' => true, 'message' => "Sync successful.", 'invoice_id' => $invoiceId], 200);

	}
}
