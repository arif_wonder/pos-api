<?php

namespace App\Http\Controllers\API;

use App\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller {

	
	/**
	 * 
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function index(Request $request) {
		
	}

	public function paymentMethod(Request $request) {

		$payments = Payment::get();
		if($payments){
			return response()->json(['status' => true, 'data' => $payments], 200);
		}
		return response()->json(['status' => false, 'message' => 'Payment method not found'], 400);
	}
}
