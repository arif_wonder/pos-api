<?php

namespace App\Http\Controllers\API;

use App\ProductBrand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class ProductBrandController extends Controller {

	public $successStatus = 200;
	/**
	 * login api
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function index(Request $request) {
		$brand = ProductBrand::get();
		return response()->json(['status' => true, 'data' => $brand], 200);	
	}

	public function add(Request $request) {

		$this->validate($request, [
			'p_name' => 'required|string|max:50',
			'p_code' => 'required|string',
			'category_id' => 'required',
			'description' => 'string',
			'p_brand' => 'required',
			'p_quantity' => 'required',
			'p_unit' => 'required',
			'p_regular_price' => 'required',
			'p_special_price' => '',
			'p_gift_item' => '',
			'p_bar_code' => '',
			'p_purchage_price' => 'required'
		]);
		$product = new Product();
				
		$product->p_name      = $request->p_name;
		$product->p_code      = $request->p_code;
		$product->category_id = $request->category_id;
		$product->description = $request->description;
		$product->p_brand     = $request->p_brand;
		$product->p_quantity  = $request->p_quantity;
		$product->p_unit      = $request->p_unit;
		$product->p_regular_price = $request->p_regular_price;
		$product->p_special_price = $request->p_special_price;
		$product->p_gift_item     = $request->p_gift_item;
		$product->p_bar_code      = $request->p_bar_code;
		$product->p_purchage_price= $request->p_purchage_price;
		
		if ($product->save()) {
			return response()->json(['status' => true, 'messages' => 'You records hasbeen inserted signup successfully', 'data' => $product], 200);
		}
	}
}
