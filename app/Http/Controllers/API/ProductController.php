<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use URL;

class ProductController extends Controller {

	public $successStatus = 200;
	/**
	 * login api
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function index(Request $request) {
		$user = \JWTAuth::user();
		if ($user && $user->user) {
			$user = $user->user;
		}
		$storeId = $user->store->store_id;
		$filters = $request->all();

		// \DB::enableQueryLog();
		$products = Product::select('products.*')
			->join('product_to_store as ps', function ($join) use ($storeId) {
				$join->on('ps.product_id', '=', 'products.p_id')
					->where('ps.store_id', '=', $storeId)
					->where('ps.quantity_in_stock', '>', 0);
			});
		// $products = $products->toSql();
		// dd($products);
		if (array_get($filters, 'p_name')) {
			$products->where(function ($query) use ($filters, $request) {
				$query->where('products.p_name', 'LIKE', '%' . $request->p_name . '%');
			});
		}
		if (array_get($filters, 'category_id')) {

			$childCategory = \App\Category::where('parent_id', $request->category_id)->pluck('category_id')->toArray();
			$category_ids = $childCategory;
			array_push($category_ids, $request->category_id);

			// $products->where(function ($query) use ($request) {
			// 	$query->where('products.category_id', $request->category_id);
			// });

			if (count($category_ids)) {
				$products->where(function ($query) use ($request, $category_ids) {
					$query->whereIn('products.category_id', $category_ids);
				});
			}
		}
		if (array_get($filters, 'p_purchage_price')) {
			$products->where(function ($query) use ($filters, $request) {
				$query->where('products.p_purchage_price', '=', $request->p_purchage_price);
			});
		}
		if (array_get($filters, 'p_regular_price')) {
			$products->where(function ($query) use ($filters, $request) {
				$query->where('products.p_regular_price', '=', $request->p_regular_price);
			});
		}
		if (array_get($filters, 'p_code')) {
			$products->where(function ($query) use ($filters, $request) {
				$query->where('products.p_code', 'LIKE', $request->p_code . '%');
			});
		}
		if (array_get($filters, 'p_bar_code')) {
			$products->where(function ($query) use ($filters, $request) {
				$query->where('products.p_bar_code', '=', $request->p_bar_code);
			});
		}
		// sorting DESC ASC
		if (array_get($filters, 'order') && array_get($filters, 'sort')) {
			$products->where(function ($query) use ($filters, $request) {
				$query->orderBy($request->sort, $request->order);
			});
		}

		$products = $products->paginate($this->getPerPage())->withPath((URL::to('/api/products/')));
		$data = $this->getPaginated($products);
		// dd(\DB::getQueryLog());
		return response()->json(['status' => true, 'data' => $data], 200);
	}

}
