<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller {

	public $successStatus = 200;
	/**
	 * login api
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function index(Request $request) {
		$supplier = Supplier::get();
		return response()->json(['status' => true, 'data' => $supplier], 200);
	}
}
