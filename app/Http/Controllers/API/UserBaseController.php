<?php

namespace App\Http\Controllers\API;

use App\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class UserBaseController extends Controller {

	public static function addUserDetails(Request $request,$user_id,$user_type) {

		$userDetail = new UserDetail();
		$userDetail->user_id, 
		$userDetail->user_type, 
		$userDetail->vehicle_number, 
		$userDetail->vehicle_model, 
		$userDetail->number_of_vehicles, 
		$userDetail->busi_name, 
		$userDetail->busi_address, 
		$userDetail->busi_contact_number, 
		$userDetail->busi_annual_turnover, 
		$userDetail->busi_trans_to_office, 
		$userDetail->ser_man_designation, 
		$userDetail->ser_man_comp_name, 
		$userDetail->ser_man_comp_address, 
		$userDetail->ser_man_comp_cont_number, 
		$userDetail->ser_man_trans_to_office, 
		$userDetail->perma_address, 
		$userDetail->perma_city, 
		$userDetail->perma_state, 
		$userDetail->perma_zip_code, 
		$userDetail->cor_address, 
		$userDetail->cor_city, 
		$userDetail->cor_state, 
		$userDetail->cor_zip_code		
		// $product->p_name      = $request->p_name;
		// $product->p_code      = $request->p_code;
		// $product->category_id = $request->category_id;
		// $product->description = $request->description;
		// $product->p_brand     = $request->p_brand;
		// $product->p_quantity  = $request->p_quantity;
		// $product->p_unit      = $request->p_unit;
		// $product->p_regular_price = $request->p_regular_price;
		// $product->p_special_price = $request->p_special_price;
		// $product->p_gift_item     = $request->p_gift_item;
		// $product->p_bar_code      = $request->p_bar_code;
		// $product->p_purchage_price= $request->p_purchage_price;
		
		// if ($product->save()) {
		// 	return response()->json(['status' => true, 'messages' => 'You have signup successfully', 'data' => $product], 200);
		// }
	}
}
