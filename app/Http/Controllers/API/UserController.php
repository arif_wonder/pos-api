<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use JWTException;

class UserController extends Controller {

	public $successStatus = 200;
	public $userToken = null;

	/**
	 * login api
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function login(Request $request) {
		Validator::make($request->all(), [
			'username' => 'required|string|email',
			'password' => 'required|string|min:6',
		]);

		$field = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'mobile';
		$credentials = [
			$field => $request->username,
			'password' => $request->password,
		];
		// \Config::set('auth.providers.users.model', \App\AuthUser::class);
		$user = \App\AuthUser::where([
			$field => $request->username,
		])->first();

		if (!$user) {
			return response()->json(['status' => false, 'message' => 'Invalid Credentials'], 401);
		}

		if (\Hash::check($request->password, $user->password)) {
			if (!$userToken = JWTAuth::fromUser($user)) {
				return response()->json(['status' => false, 'message' => 'Invalid Credentials'], 401);
			}

			if ($user->client && !$user->client->active) {
				return response()->json(['status' => false, 'message' => 'Your account is deactivated.'], 401);
			}
			$user->api_token = $userToken;
			$user->save();
			$user['token'] = $userToken;

			$detect = new \App\Helpers\MobileDetect;
			\App\Helpers\MyDBHelper::setDBConnection($user);
			$userOwn = $user->user;
			$perm = [];
			if ($userOwn) {
				$ugrp = \DB::table('user_group')->where('group_id', $userOwn->group_id)->first();
				$perm = unserialize($ugrp->permission);
				$user['manage_pos'] = (isset($perm['access']['manage_pos']) && $perm['access']['manage_pos'] == 'true') ? true : false;
				$user['manage_invoice'] = false;
				$invRead = (isset($perm['access']['read_invoice_list']) && $perm['access']['read_invoice_list'] == 'true') ? true : false;
				$invUpdate = (isset($perm['access']['update_invoice']) && $perm['access']['update_invoice'] == 'true') ? true : false;
				if ($invRead && $invUpdate) {
					$user['manage_invoice'] = true;
				}
			}

			if ($detect->isMobile() || $detect->isTablet()) {
				if ($userOwn && (is_array($perm) && count($perm))) {
					if (isset($perm['access']['manage_pos'])
						&& $perm['access']['manage_pos'] == 'true') {
						return response()->json(['status' => true, 'data' => $user], $this->successStatus);
					}
				}
				return response()->json(['status' => false, 'message' => 'You don\'t have enough permissions.'], 401);
			}

			return response()->json(['status' => true, 'data' => $user->makeHidden('user')], $this->successStatus);
		}
		return response()->json(['status' => false, 'message' => 'Invalid credentials.'], 401);
	}

	public function logout(Request $request) {
		$token = JWTAuth::getToken();
		$loginUser = JWTAuth::user();
		try {
			if ($loginUser) {
				$loginUser->api_token = '';
				$loginUser->save();
			}
			JWTAuth::invalidate($token);
			return response()->json([
				'status' => 'success',
				'message' => "User successfully logged out.",
			]);
		} catch (JWTException $e) {
			return response()->json([
				'status' => 'error',
				'message' => 'Failed to logout, please try again.',
			], 500);
		}
	}

	public function register(Request $request) {

		$this->validate($request, [
			'username' => 'required|string|max:50',
			'email' => 'required|string|email|max:255|unique:users',
			'mobile' => 'required|digits:10|unique:users',
			'password' => 'required|min:6|max:15',
			'group_id' => 'required|digits:1',
		]);
		$user = new User();
		$user->username = $request->username;
		$user->email = $request->email;
		$user->mobile = $request->mobile;
		$user->password = md5($request->password);
		$user->raw_password = $request->password;
		$user->group_id = $request->group_id;
		$user->created_at = date('Y-m-d H:i:s');

		if ($user->save()) {
			return response()->json(['status' => true, 'messages' => 'Signed up successfully', 'data' => $user], 200);
		} else {
			return response()->json(['status' => false, 'messages' => 'There is something wrong.'], 400);
		}
	}

	public function getStore(Request $request) {
		$user = \JWTAuth::user();
		if ($user && $user->user) {
			$user = $user->user;
		}

		if ($user->store && ($store = $user->store->store)) {
			return response()->json(['status' => true, 'data' => $store], 200);
		}
		return response()->json(['status' => false, 'message' => 'Store not found'], 422);
	}
}
