<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\UsersBank;
use Illuminate\Http\Request;

class UsersBankController extends Controller {

	public $successStatus = 200;
	/**
	 * login api
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function index(Request $request) {
		$category = UsersBank::get();
		return response()->json(['status' => true, 'data' => $category], 200);
	}

	public function add(Request $request) {
	}
}
