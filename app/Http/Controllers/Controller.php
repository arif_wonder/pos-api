<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Request;

class Controller extends BaseController {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function getPaginated($paginatedCollection) {
		return collect([
			'pagination' => [
				'total' => $paginatedCollection->total(),
				'per_page' => $paginatedCollection->perPage(),
				'current_page' => $paginatedCollection->currentPage(),
				'last_page' => $paginatedCollection->lastPage(),
				'next_page_url' => $paginatedCollection->nextPageUrl(),
				'prev_page_url' => $paginatedCollection->previousPageUrl(),
				'from' => ($paginatedCollection->currentPage() <= 1) ? $paginatedCollection->currentPage() : ($paginatedCollection->currentPage() - 1) * $paginatedCollection->perPage() + 1,
				'to' => min($paginatedCollection->total(), $paginatedCollection->currentPage() * $paginatedCollection->perPage()),
			],
			'results' => $paginatedCollection->getCollection(),
		]);
	}

	/**
	 * This performs CallingModel::getPerPage(). Is able to be chained.
	 * @return mixed
	 */
	public static function getPerPage() {
		return Request::get('per_page', 50);
	}
}
