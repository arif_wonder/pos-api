<?php

namespace App\Http\Middleware;

use App\Helpers\MyDBHelper;
use Closure;

class SetDBConn {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		MyDBHelper::setDBConnection();
		return $next($request);
	}
}
