<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class P2P_Notif extends Model {
	protected $connection = 'mysql';

	protected $table = 'p2p_notif';
	protected $fillable = ['notif_id'];
	public $timestamps = false;
}
