<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model {
	protected $connection = 'mysql';

	public $timestamps = false;
	protected $fillable = ['name', 'details', 'created_at'];
	protected $primaryKey = 'payment_id';
}
