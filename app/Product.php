<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {
    protected $connection = 'mysql';
	public $timestamps = false;
	protected $primaryKey = "p_id";
    private $discountedPrice;

    public function __construct(array $attributes = [])
    {
        $this->discountedPrice = 0;
        parent::__construct($attributes);
    }

	protected $fillable = [
		'p_name', 'p_code', 'category_id', 'description', 'p_brand', 'p_quantity', 'p_unit', 'p_regular_price', 'p_special_price', 'p_gift_item', 'p_bar_code', 'p_purchage_price', 'p_discount', 'p_discount_type',
	];
	protected $appends = ['avl_quantity', 'selling_price', 'cat_discount', 'hh_discount', 'price_after_disc'];
	// protected $appends = ['avl_quantity', 'selling_price'];
	protected $hidden = ['category'];

	public function buyingItem() {
		return $this->hasOne('App\BuyingItem', 'item_id', 'p_id')->orderBy('id', 'DESC');
	}

	public function category() {
		return $this->belongsTo('App\Category', 'category_id', 'category_id');
    }

    public function getPriceAfterDiscAttribute() {
        // dd($this->discountedPrice);
        return $this->discountedPrice; // < 0 ? 0 : 1;
    }

	public function getPDiscountAttribute() {
		$discount = 0;
		if ($this->p_discount_type == 'fixed') {
			$discount = $this->attributes['p_discount'];
		} else if ($this->p_discount_type == 'precentage') {
			$discount = ($this->attributes['p_discount'] * $this->selling_price) / 100;
        }
        $this->discountedPrice = $this->selling_price - $discount;
        return $discount;
    }

    public function getCatDiscountAttribute() {
        return $this->calcCatDiscount();
    }

    public function getHhDiscountAttribute() {
        return $this->calcHHDiscount();
    }

	public function calcCatDiscount() {
		$cat = $this->category;
        $discount = [];
		if ($cat) {
			$discount = $disc = $cat->discount;
			if ($disc) {
				$discount['amount'] = 0;
				if (\Carbon\Carbon::now()->between(\Carbon\Carbon::parse($disc->date_from), \Carbon\Carbon::parse($disc->date_to . ' ' . '23:59:59'))) {
					if ($this->selling_price >= $disc->min_purchase_amount) {
                        if ($disc->discount_type == 'precentage') {
                            $amt = ($this->discountedPrice * $disc->discount) / 100;
                            if ((int) $disc->max_discount) {
                                $discount['amount'] = min($amt, $disc->max_discount);
                            } else {
                                $discount['amount'] = $amt;
                            }
                        } elseif ($disc->discount_type == 'fixed') {
                            $discount['amount'] = $disc->discount;
                        }
                        $this->discountedPrice -= $discount['amount'];
                        $discount['discounted_price'] = $this->discountedPrice;
                    }
				}
				return $discount['amount'] ? $discount : null;
            }
            return null;
		}
	}

	public function calcHHDiscount() {
		$day = strtolower(date('D'));
        // \DB::enableQueryLog();
		$discount = $disc = \App\DiscountHappyHour::whereRaw('FIND_IN_SET(?, on_days)', [$day])
			->where(function ($query) {
                $query->whereRaw('time_from <= CURRENT_TIME() AND time_to >= CURRENT_TIME()');
			})
            ->first();
        // dd(\DB::getQueryLog());

		if ($disc) {
			if ($this->selling_price >= $disc->min_purchase_amount) {
                if ($disc->discount_type == 'precentage') {
                    $amt = ($this->discountedPrice * $disc->discount) / 100;
                    if ((int) $disc->max_discount) {
                        $discount['amount'] = min($amt, $disc->max_discount);
                    } else {
                        $discount['amount'] = $amt;
                    }
                } elseif ($disc->discount_type == 'fixed') {
                    $discount['amount'] = $disc->discount;
                }
                $this->discountedPrice -= $discount['amount'];
                $discount['discounted_price'] = $this->discountedPrice;
                return $discount;
            }
        }
        return null;
	}

	public function getPQuantityAttribute() {
		return $this->avl_quantity;
	}

	public function getSellingPriceAttribute() {
		$prd = $this->buyingItem;
		$prd = $prd ? $prd->toArray() : null;
		return $prd ? $prd['item_selling_price'] : 0;
	}

	public function getAvlQuantityAttribute() {
		$user = \JWTAuth::user();
		if ($user && $user->user) {
			$user = $user->user;
		}
		$prdQty = \DB::table('product_to_store')
			->select('quantity_in_stock')
			->where([
				'store_id' => $user->store->store_id,
				'product_id' => $this->p_id,
			])->value('quantity_in_stock');
		return $prdQty ?: 0;
	}

	public function getPTaxrateAttribute() {
		$query = \DB::table('tax_rates')->where('tax_rate_id', $this->attributes['p_taxrate'])->first();
		return $query && $query->rate ? $query->rate : 0;
	}
}
