<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturnProduct extends Model {
	protected $connection = 'mysql';

	protected $fillable = ['quantity', 'info_id', 'invoice_id', 'product_id', 'customer_id', 'user_id', 'store_id', 'comments', 'image', 'ret_amount', 'return_date'];
	protected $table = 'return_product_info';
	protected $primaryKey = 'return_id';
	public $timestamps = false;
	protected $dates = ['return_date'];
}
