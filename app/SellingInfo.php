<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellingInfo extends Model {
	protected $connection = 'mysql';

	protected $table = 'selling_info';

	protected $primaryKey = 'info_id';

	protected $guarded = [];
	protected $appends = ['cashier', 'gross_total'];

	public function sellingItems() {
		return $this->hasMany('App\SellingItem', 'invoice_id', 'invoice_id');
	}

	public function sellingPrice() {
		return $this->hasOne('App\SellingPrice', 'invoice_id', 'invoice_id');
	}

	public function getCartDataAttribute($cartData) {
		return json_decode($cartData);
	}

	public function getCashierAttribute() {
		$user = \App\User::find($this->attributes['created_by']);
		return $user ? $user->username : '';
	}

	public function getGrossTotalAttribute() {
		$items = $this->sellingItems;
        $total = 0;
        if ($items) {
            // dd($items->toArray());
            foreach ($items as $item) {
                $total += $item->item_total;
            }
        }
		return $total;
	}
}
