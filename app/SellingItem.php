<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellingItem extends Model {
	protected $connection = 'mysql';

	protected $table = 'selling_item';

	protected $guarded = [];
	protected $appends = ['product', 'price_after_discount'];

	public $timestamps = false;

	public function sellingInfo() {
		return $this->hasOne('App\SellingInfo', 'invoice_id', 'invoice_id');
	}

	public function product() {
		return $this->hasOne('App\Product', 'p_id', 'item_id');
	}

	public function getProductAttribute() {
		return $this->product()->first();
	}

	public function getPriceAfterDiscountAttribute() {
		return $this->item_total - $this->item_discount;
	}
}
