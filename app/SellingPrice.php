<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellingPrice extends Model {
	protected $connection = 'mysql';

	protected $table = 'selling_price';

	protected $primaryKey = 'price_id';

	protected $guarded = [];

	public $timestamps = false;
}
