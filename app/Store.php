<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model {
	protected $connection = 'mysql';

    protected $guarded = [];

    public function state()
    {
        return $this->hasOne('App\State', 'id', 'state');
    }

    public function getPreferenceAttribute($pref)
    {
        return unserialize($pref);
    }

    public function getStateAttribute($stateId)
    {
        $state =  \DB::table('states')->select('name')->where('id', $stateId)->first();
        return $state ? $state->name : null;
    }
}
