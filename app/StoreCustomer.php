<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreCustomer extends Model {
	protected $connection = 'mysql';

	protected $guarded = [];
	protected $table = 'customer_to_store';
	protected $primaryKey = 'c2s_id';
	public $timestamps = false;
}
