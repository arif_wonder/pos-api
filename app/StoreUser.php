<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreUser extends Model {
	protected $connection = 'mysql';

	protected $table = 'user_to_store';

	protected $primaryKey = 'u2s_id';

	protected $guarded = [];

	public function users() {
		return $this->hasMany('App\User', 'user_id', 'store_id');
	}

	public function store() {
		return $this->hasOne('App\Store', 'store_id', 'store_id');
	}
}
