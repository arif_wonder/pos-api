<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject {
	use Notifiable;
	protected $connection = 'mysql';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'username', 'email', 'mobile', 'password', 'raw_password', 'group_id', 'device_id', 'device_type'
	];
	protected $appends = [''];
	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'api_token', 'raw_password',
	];

	public function getJWTIdentifier() {
		return $this->getKey();
	}

	public function getJWTCustomClaims() {
		return [];
	}

	public function store() {
		return $this->hasOne('App\StoreUser', 'user_id');
	}

	public function cart() {
		return $this->hasOne('App\Cart', 'user_id');
	}
}
