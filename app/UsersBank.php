<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersBank extends Model {
	protected $connection = 'mysql';

	protected $table = 'users_bank';
}
