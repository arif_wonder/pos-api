<?php

use Illuminate\Http\Request;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Authorization,Content-Type, Accept, x-xsrf-token");

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
Route::get('/', function () {
	return view('welcome');
});
Route::get('/password', function (Request $request) {
	return response()->json(['crypt_password' => bcrypt($request->password)]);
});
Route::group(['namespace' => 'API'], function () {
	Route::post('/login', 'UserController@login');
	Route::post('/signup', 'UserController@register');
	Route::post('/test-p2p', 'MessageController@testP2P');
	// ************** after login ************** //
	Route::group(['middleware' => ['auth:api', 'dbcon']], function () {
		Route::post('/logout', 'UserController@logout');
		Route::get('/store', 'UserController@getStore');
		/* all suppliers functions */
		Route::group(['prefix' => 'suppliers'], function () {
			Route::get('/', ['as' => 'api.suppliers.index', 'uses' => 'SupplierController@index']);
			Route::post('/add', ['as' => 'api.suppliers.add', 'uses' => 'SupplierController@add']);
		});
		/* all customers functions */
		Route::group(['prefix' => 'customer'], function () {
			Route::get('/', ['as' => 'api.customer.index', 'uses' => 'CustomerController@index']);
			Route::post('/create', ['as' => 'api.customer.add', 'uses' => 'CustomerController@create']);
		});
		/* all category functions */
		Route::group(['prefix' => 'categories'], function () {
			Route::get('/', ['as' => 'api.categories.index', 'uses' => 'CategoryController@index']);
			Route::post('/add', ['as' => 'api.categories.add', 'uses' => 'CategoryController@add']);
		});
		/* all product_brands functions */
		Route::group(['prefix' => 'product_brands'], function () {
			Route::get('/', ['as' => 'api.product_brands.index', 'uses' => 'ProductBrandController@index']);
			Route::post('/add', ['as' => 'api.product_brands.add', 'uses' => 'ProductBrandController@add']);
		});
		/* Product Group */
		Route::group(['prefix' => 'products'], function () {
			Route::any('/', ['as' => 'api.products.index', 'uses' => 'ProductController@index']);
		});

		/* Cart Group */
		Route::group(['prefix' => 'cart'], function () {
			Route::get('/', ['as' => 'api.products.index', 'uses' => 'ProductController@index']);
			Route::post('/add', ['as' => 'api.cart.add', 'uses' => 'CartController@addToCarts']);
			Route::post('/decrease', ['as' => 'api.cart.add', 'uses' => 'CartController@decreaseCart']);
			Route::post('/empty', ['as' => 'api.cart.add', 'uses' => 'CartController@emptyCart']);
			// Route::get('/getamount', ['as' => 'api.cart.add', 'uses' => 'CartController@getPayableAmount']);
			Route::post('/apply-coupon', ['as' => 'api.cart.add', 'uses' => 'CheckoutController@applyCoupon']);
		});

		/* User Mine group*/
		Route::group(['prefix' => 'mine'], function () {
			Route::get('/cart', ['as' => 'api.cart.add', 'uses' => 'CartController@index']);
			Route::post('/cart/product/remove', ['as' => 'api.cart.remove', 'uses' => 'CartController@removeCart']);
		});

		//payments route
		Route::group(['prefix' => 'payments'], function () {
			Route::get('/method', ['as' => 'api.payments.method', 'uses' => 'PaymentController@paymentMethod']);
			Route::post('/order', ['as' => 'api.payments.order', 'uses' => 'CheckoutController@placeOrder']);
		});

        /* Route::get('/inc-credit', function(Request $request) {
            // return $user = JWTAuth::user();

            $cust = \App\Customer::find(2);
            $cust->credit_balance = 100;
            $cust->save();
            return $cust;
        }); */
		// Invoice group
		Route::group(['prefix' => 'invoice'], function () {
			Route::get('/all', ['as' => 'api.invoice.all', 'uses' => 'InvoiceController@index']);
			Route::get('/get/{invid}', ['as' => 'api.invoice.get', 'uses' => 'InvoiceController@getInvoiceData']);
			Route::post('/return', ['as' => 'api.invoice.get', 'uses' => 'InvoiceController@returnItem']);
		});

		// Message group
		Route::group(['prefix' => 'message'], function () {
			Route::post('/bulk/send', ['as' => 'api.message.bulk.send', 'uses' => 'MessageController@bulkSend']);
			Route::post('/invoice/send', ['as' => 'api.message.sms.send', 'uses' => 'MessageController@sendInvoiceSMS']);
			Route::post('/invoice/send/wa', ['as' => 'api.message.wa.send', 'uses' => 'MessageController@sendInvoiceWA']);
			Route::post('/p2p/send', ['as' => 'api.message.p2p.send', 'uses' => 'MessageController@sendP2PNotification']);
			Route::get('/p2p/get-notifid', ['as' => 'api.message.getnotifid', 'uses' => 'MessageController@getP2PNotifId']);
		});

		Route::group(['prefix' => 'offline'], function () {
			Route::post('/orders/sync', ['as' => 'api.fllineorder.sync', 'uses' => 'OfflineOrderController@syncOrders']);
		});

		Route::get('/test-inv', 'TestController@index');

	});
});

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
return $request->user();
}); */
